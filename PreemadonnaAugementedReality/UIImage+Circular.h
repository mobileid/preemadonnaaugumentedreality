//
//  UIImage+Circular.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 05/10/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Circular)
- (UIImage*)circularScaleAndCropImage:(UIImage*)image frame:(CGRect)frame ;

@end
