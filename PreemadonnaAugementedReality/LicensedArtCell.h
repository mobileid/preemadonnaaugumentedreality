//
//  LicensedArtCell.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"BaseCollectionViewCell.h"
@interface LicensedArtCell : BaseCollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UIButton *buttonPurchseFlag;
@property(strong, nonatomic) void (^purchaseHandler)(BOOL *status);

@end
