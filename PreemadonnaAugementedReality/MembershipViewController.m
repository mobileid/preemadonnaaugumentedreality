//
//  MembershipViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "MembershipViewController.h"

@interface MembershipViewController ()

@end

@implementation MembershipViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setScreeNname:@"Membership Screen"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionFreeMembership:(id)sender {
    [self performSegueWithIdentifier:@"toHome" sender:sender];
}
- (IBAction)actionArtistMembership:(id)sender {
    [self performSegueWithIdentifier:@"toHome" sender:sender];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
