//
//  GalleryViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "GalleryViewController.h"
#import "GalleryCell.h"
#import "nailFindingViewController.h"
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import "GAI.h"
#import "GAITrackedViewController.h"
#import "Preemadonna.h"
#import <POP.h>

@interface GalleryViewController () <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    IBOutlet UICollectionView *galleryCollectionView;
    IBOutlet UIPageControl *pageControll;
    IBOutlet UIButton *buttonNext;
    GalleryCell * prevCell;
    GalleryCell * galleryCell;
    BOOL isAnimationFirstTime;
    BOOL isDecresedCelSize;
    NSMutableArray * imageArray;
    IBOutlet UILabel *headerLabel;
    NSMutableArray * headerArray;
    NSMutableArray * holidayImageArray;
    NSMutableArray * datasource;
    CAGradientLayer *gradient;
}

@end

@implementation GalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    galleryCollectionView.delegate = self;
    galleryCollectionView.dataSource = self;
    buttonNext .enabled = NO;
    buttonNext.alpha  = 4.0;
    NSError *error = nil;
    NSString *yourFolderPath = [[[NSBundle mainBundle] resourcePath]
                                stringByAppendingPathComponent:@"36x36"];
    
    NSArray  *yourFolderContents = [[NSFileManager defaultManager]
                                    contentsOfDirectoryAtPath:yourFolderPath error:&error];
//    NSLog(@"directoryContents ====== %@",yourFolderContents);
    
//    imageArray = [[NSMutableArray alloc] initWithArray:yourFolderContents];
//    imageArray = [[NSMutableArray alloc] initWithObjects:@"usa", @"wink", @"snowman", @"ambulance", @"crown", @"camera",@"gears",@"keys",@"bee",@"shoe",@"snail",@"carousel",@"music",@"surf",@"pizza",@"tool",@"money",@"stadium",@"beach",@"watermelon",@"aquarius",@"aries",@"baseball",@"liberty",@"burger",@"cat2",@"club",@"kiss",@"clover",@"bow",@"cat",@"chocolate",@"church",@"cityscape",@"firetruck",@"cake",@"chick",@"cancer",@"cross",@"diamond",@"flower2",@"capricon",@"gem",@"dolphin",@"smile1",@"fries",@"turtle",@"gemini",@"lipstick",@"lolipop",@"heart1",@"fish",@"hen",@"monkey",@"heart2",@"leaf",@"ladybug",@"mushroom",@"flower3",@"pig",@"heart",@"bear",@"football",@"turkey",@"hourglass",@"joker",@"smile2",@"bolt",@"pisces",@"sun2",@"pineapple",@"ribbon",@"pumpkin",@"libra",@"lips",@"snowflake",@"police",@"bulb",@"leo",@"masks",@"mic",@"octopus",@"paws",@"micorphone",@"sun",@"taurus",@"sagittarius",@"save",@"strawberry",@"star",@"spade",@"shop",@"scorpio",@"train",@"umbrella",@"virgo",@"fall", @"halloween", @"maple", @"thanksgiving1", @"thanksgiving2", @"hannukah",@"star3",@"snow",@"snowman",@"star1",@"santa",@"xmas",nil];
    imageArray = [[NSMutableArray alloc] init];
    for (NSString * string in yourFolderContents) {
//        [imageArray addObject:[string stringByReplacingOccurrencesOfString:@".png" withString:@""]];
        [imageArray addObject:[NSString stringWithFormat:@"%@",(NSString*)string]];
    }
    
    pageControll.numberOfPages = 10;
    [self setScreeNname:@"Gallery Screen"];
    headerArray = [[NSMutableArray alloc] initWithObjects:@"Gallery",@"Holiday collection",nil];
    headerLabel.text = headerArray[pageControll.currentPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDelegate&DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"GalleryCell";
    GalleryCell *cell = [galleryCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    NSString *yourFolderPath = [[[NSBundle mainBundle] resourcePath]
                                stringByAppendingPathComponent:[NSString stringWithFormat:@"36x36/%@",imageArray[indexPath.row]]];
    UIImage * image = [UIImage imageWithContentsOfFile:yourFolderPath];
    cell.iconImageView.image = [UIImage imageWithContentsOfFile:yourFolderPath];
    cell.iconImageView.image.accessibilityIdentifier = imageArray[indexPath.row];
    cell.tag = indexPath.row;
    return cell;
}

//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
//{
//    UICollectionReusableView *reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
//    if (reusableView == nil) {
//        reusableView = [[UICollectionReusableView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    }
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [reusableView addSubview:label];
//    return reusableView;
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    GalleryCell * cell = (GalleryCell*)[galleryCollectionView cellForItemAtIndexPath:indexPath];
    galleryCell  = cell;
    buttonNext.enabled = YES;
    buttonNext.alpha = 1.0;
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:[GAIFields customDimensionForIndex:1] value:[Preemadonna sharedClient].curruntUser.objectId];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action" action:@"ArtSelection" label:cell.iconImageView.image.accessibilityIdentifier value:nil]build]];
    
    //Animation
    if (prevCell.tag != cell.tag){
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration = 0.1;
        scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1.40, 1.40)];
        [cell pop_addAnimation:scaleAnimation forKey:@"scalingUp"];
        
        POPSpringAnimation *sprintAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        sprintAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(0.9, 0.9)];
        sprintAnimation.velocity = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
        sprintAnimation.springBounciness = 20.f;
        [cell.iconImageView pop_addAnimation:sprintAnimation forKey:@"springAnimation"];
        
        if (prevCell != nil){
            POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
            scaleAnimation.duration = 0.1;
            scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
            [prevCell pop_addAnimation:scaleAnimation forKey:@"scalingDown"];
        }
        prevCell = cell;
        
    }else{
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration = 0.1;
        scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
        [cell pop_addAnimation:scaleAnimation forKey:@"scalingDown"];
        buttonNext.enabled = NO;
        buttonNext.alpha = 0.4;
        galleryCell = nil;
        prevCell = nil;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(50, 50);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat width   = CGRectGetWidth(galleryCollectionView.frame);
    pageControll.currentPage = (galleryCollectionView.contentOffset.x/width);
}

#pragma mark - IBAction
- (IBAction)actionNext:(id)sender {
    if (galleryCell != nil) {
        [self performSegueWithIdentifier:@"toCamera" sender:galleryCell];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"toCamera"]) {
        nailFindingViewController * destinationViewController = [segue destinationViewController];
        destinationViewController.defaultNailUIImage = galleryCell.iconImageView.image;
    }
}

@end
