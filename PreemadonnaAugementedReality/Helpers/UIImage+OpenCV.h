//
//  UIImage+OpenCV.h
//  OpenCVClient
//
//  Created by Robin Summerhill on 02/09/2011.
//  Copyright 2011 Aptogo Limited. All rights reserved.
//
//  Permission is given to use this source code file without charge in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

// Modified by Stan - Removed "autorelase" for ARC
// Other possibility if this category is troublesome: http://stackoverflow.com/questions/8563356/nsimage-to-cvmat-and-vice-versa

// Might want to integrate these tools, as they seem faster with color conversion: https://github.com/jonmarimba/OpenCV-iOS/tree/master/OpenCV-iOS-Tools

#import <UIKit/UIKit.h>
#import <opencv2/opencv.hpp>

@interface UIImage (UIImage_OpenCV)

+(UIImage *)imageWithCVMat:(const cv::Mat&)cvMat;
-(id)initWithCVMat:(const cv::Mat&)cvMat;

@property(nonatomic, readonly) cv::Mat CVMat;
@property(nonatomic, readonly) cv::Mat CVGrayscaleMat;

@end