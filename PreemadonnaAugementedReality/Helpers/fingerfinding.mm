//
//  fingerfinding.cpp
//  Nail Finding Tester
//
//  Created by Stan James on 6/30/15.
//

#include "fingerfinding.h"
#import <Tweaks/FBTweak.h>
#import <Tweaks/FBTweakInline.h>
#include "cvtoolbox.h"


void fingerfinding::findRedAreas(double redThreshold, cv::Mat src, cv::Mat& dst, cv::Mat& displayImage) {
    
    cv::Mat channels[3];
    cv::split(src, channels);
    // in our messed up world, red is in channels[0]
    cv::Mat red = channels[0];
    cv::Mat green = channels[1];
    cv::Mat blue = channels[2];
    
    bool useBlueAsDiff = FBTweakValue(@"Nail Finding", @"Finger", @"BlueDif=On GreenDif=Off", YES);
    if (useBlueAsDiff) {
        dst = red - blue;
    }
    else {
        dst = red - green;
    }
    
    if (FBTweakValue(@"Nail Finding", @"Finger", @"Normalize Red Dif", NO) && !displayImage.empty()) {
        cv::normalize(dst, dst, 0, 255, cv::NORM_MINMAX);
    }
    
    cv::threshold(dst, dst, redThreshold, 255, cv::THRESH_BINARY);
    
    // This gives a fuzzy dim-ish view of where the finger is, but with background noise
    // and shadows removed.
    
    // Debug display
    if (FBTweakValue(@"Nail Finding", @"Display", @"redFingerImage", NO)  && !displayImage.empty()) {
        displayImage.setTo(cv::Scalar(255,0,0), dst);
    }
    if (FBTweakValue(@"Nail Finding", @"Display", @"Red Difference", NO) && !displayImage.empty()) {
        cv::Mat redFingerImage3Channel;
        cv::cvtColor(dst, redFingerImage3Channel, CV_GRAY2BGR);
        displayImage += redFingerImage3Channel;
    }
    
//    double meanFingerBrightness = cv::mean(grayImage, redFingerImage)[0];
    
}

/**
 * Find fingernail in image
 */
bool fingerfinding::findFingerNail(cv::Mat src, cv::Mat& nailMat, std::vector<cv::Point>& nailContour, cv::Rect& nailBoundingRect, cv::Mat& displayImage)
{
    findFingerNailSteps steps; // declare default container for intermediate steps
    return fingerfinding::findFingerNail(src, nailMat, nailContour, nailBoundingRect, displayImage, steps);
}

bool fingerfinding::findFingerNail(cv::Mat src, cv::Mat& nailMat, std::vector<cv::Point>& nailContour, cv::Rect& nailBoundingRect, cv::Mat& displayImage, findFingerNailSteps& steps)
{
    cv::cvtColor(src, steps.grayImage, CV_BGRA2GRAY);
    bool continueProcessing = true;
    
    // Find the best finger contour
    double meanFingerBrightness = 0.0;
    double fingerSkinContourRoughness = 100.0;
    std::vector<cv::Point> fingerSkinContour;
    cv::Rect fingerSkinBoundingRect = cv::Rect(0,0,0,0);
    int fingerRedThreshold;
    
    for (fingerRedThreshold = 4; fingerRedThreshold < 100; fingerRedThreshold += 4) {
        
        fingerSkinContour.clear();
        
        // Find redish (skin-ish) areas of image
        fingerfinding::findRedAreas(fingerRedThreshold, src, steps.redFingerImage, displayImage);
        
        meanFingerBrightness = cv::mean(steps.grayImage, steps.redFingerImage)[0];
        
        // Find biggest contour of skin
        std::vector<std::vector<cv::Point> > fingerSkinContours;
        int biggestContourIndex = -1;
        cv::Mat redFingerCopy = steps.redFingerImage.clone();
        cv::findContours(redFingerCopy, fingerSkinContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
        
        biggestContourIndex = cvt::findBiggestContourIndex(fingerSkinContours);
        
        if (biggestContourIndex >= 0) {
            fingerSkinContour = fingerSkinContours[biggestContourIndex];
            fingerSkinBoundingRect = cv::boundingRect(fingerSkinContour);
        }
        
        if ((fingerSkinContour.size() > 0) && biggestContourIndex) {
            // We have a potential finger contour
            
            // Evaluate for convexity
            std::vector<cv::Point> hull;
            cv::convexHull(fingerSkinContour, hull);
            
            double rawPerimeter = cv::arcLength(fingerSkinContour, true);
            double hullPerimeter = cv::arcLength(hull, true);
            fingerSkinContourRoughness = rawPerimeter / hullPerimeter;
            
            // Show it
            
            // See if our "finger" extends the whole width/height of image
            // (with 5 pixel fudge factor)
            bool isTooBig = ((fingerSkinBoundingRect.size().width  >= (src.cols - 5)) ||
                             (fingerSkinBoundingRect.size().height >= (src.rows - 5)));
            
            bool isTooSmall = ((fingerSkinBoundingRect.area()*100 / src.size().area()) < 10); // require min of 10% of image be finger
            
            double maxFingerRoughness = FBTweakValue(@"Nail Finding", @"Finger", @"maxFingerRoughness", 1.2, 0.5, 4.00);
            bool isSmoothEngough = (fingerSkinContourRoughness < maxFingerRoughness);
            
            if (isSmoothEngough && !isTooBig && !isTooSmall) {
                if (FBTweakValue(@"Nail Finding", @"Display", @"Finger Contour", YES)) {
                    cv::drawContours(displayImage, fingerSkinContours, biggestContourIndex, cv::Scalar(255,0,0), 2);
                }
                
                // stop our searching -- this will be our finger
                break;
            }
            
        }
    }
    
    if (fingerSkinContour.size() <= 0) {
        // We didn't find a finger. We're done here.
        return false;
    }
    
    //
    // Find pixels likely to belong to the nail, and set in fingerThresholdImage
    // TODO: Abstract to own function
    //
    {
        // Look for color-less areas of the image (i.e. white/gray/black)
        // DEBUG
        cv::Mat imageHSV;
        cv::cvtColor(src, imageHSV, CV_BGR2HSV);
        cv::Mat channels[3];
        cv::split(imageHSV, channels);
        cv::Mat saturationMat = channels[1];
        // A pixel can have a saturation of AT MOST this much to be included as possible nail
        int saturationThreshold = FBTweakValue(@"Nail Finding", @"Nail", @"Max Saturation", 10, 0, 255);
        cv::threshold(saturationMat, saturationMat, saturationThreshold, 255, CV_THRESH_BINARY_INV);
        // At this point, white pixels indicate potential nail areas (based on saturation)
        
        // Find pixels that are at least as *bright* as finger ... this should include the nail
        cv::Mat brightMat;
        int brightnessThreshholdShift = FBTweakValue(@"Nail Finding", @"Nail", @"Brightness thresh shift", -30, -100, +100);
        double fingerLightThreshold = MAX(MIN(meanFingerBrightness + brightnessThreshholdShift, 255), 0); // Must be at least as bright as the finger itself
        cv::threshold(steps.grayImage, brightMat, fingerLightThreshold, 255, cv::THRESH_BINARY);
        
        cv::bitwise_and(brightMat, saturationMat, steps.potentialNailPixelsMat);
        
        // might need to add an erode step (but only if contour finding is slowing us down too much)
        int dilation_size = FBTweakValue(@"Nail Finding", @"Finger", @"Dilate", 3, 1, 10);
        cv::Mat dilationElement = getStructuringElement(cv::MORPH_RECT,
                                                        cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                                        cv::Point( dilation_size, dilation_size ) );
        cv::dilate(steps.potentialNailPixelsMat, steps.potentialNailPixelsMat, dilationElement);
        
        // Debug display
        if (FBTweakValue(@"Nail Finding", @"Display", @"finger threshold", NO)) {
            displayImage.setTo(cv::Scalar(127,0,0), steps.potentialNailPixelsMat);
        }
        // Debug display
        if (FBTweakValue(@"Nail Finding", @"Display", @"finger bright", NO)) {
            displayImage.setTo(cv::Scalar(0,127,0), brightMat);
        }
        // Debug display
        if (FBTweakValue(@"Nail Finding", @"Display", @"finger saturation", NO)) {
            displayImage.setTo(cv::Scalar(0,0,127), saturationMat);
        }
        
    }
    
    // Find bright contours that overlap with finger -- this should include the nail
    // (and exclude noise)
    {
        steps.potentialNailContoursMat = cv::Mat::zeros(src.size(), CV_8UC1);
        
        // Find contours in our mat of bright, unsaturated areas
        std::vector<std::vector<cv::Point> > brightContours;
        if (fingerSkinContour.size() > 0) {
            cv::Mat fingerThresholdImageCopy = steps.potentialNailPixelsMat.clone();
            cv::findContours(fingerThresholdImageCopy, brightContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
            
            int biggestOverlappingIndex = -1;
            double biggestOverlappingSize = 0;
            
            for( size_t i = 0; i < brightContours.size(); i++ ) {
                // for now we use simple bounding boxes. we could get fancier and test for overlap of the actual
                // pixels in the contours.
                cv::Rect brightBoundRect = cv::boundingRect(brightContours[i]);
                
                cv::Point brightBoundCenter = cv::Point(brightBoundRect.x + (brightBoundRect.width/2),
                                                        brightBoundRect.y + (brightBoundRect.height/2));
                
                // Skip contours obivously too big
                //                        if (brightBoundRect.width > (image.cols / 2)) {
                //                            continue;
                //                        }
                
                // See: http://opencv.itseez.com/modules/core/doc/basic_structures.html?highlight=rect#Rect_
                cv::Rect intersection = (brightBoundRect & fingerSkinBoundingRect);
                double polyArea = cv::contourArea(brightContours[i]);
                
                // Does contour intersect our finger region?
                bool doesIntersect = (intersection.width > 0);
                // Is it bigger than any valid contour we've seen so far?
                bool isBiggest = (polyArea > biggestOverlappingSize);
                // Does it touch the bottom of the image? (means likely to be noise) -- This had too many false positives
                //bool touchesBottom = (brightBoundRect.y+brightBoundRect.height) >= (image.rows - 5);
                // Does the the x centerpoint of our nail lie within widht of our finger? (i.e. not hanging off the side?)
                bool isOverFinger = (brightBoundCenter.x > fingerSkinBoundingRect.x) && (brightBoundCenter.x < (fingerSkinBoundingRect.x+fingerSkinBoundingRect.width));
                // Is the nail rectangle smaller that some percent of our finger?
                bool isNotTooWide = (brightBoundRect.width < (fingerSkinBoundingRect.width * 1.2));
                
                bool doBrightContourDebugDisplay = (FBTweakValue(@"Nail Finding", @"Display", @"Bright contours", NO));
                
                // Debug display
                if (doBrightContourDebugDisplay) {
                    cv::drawContours(displayImage, brightContours, i, cv::Scalar(0,0,0), 4);
                    cv::drawContours(displayImage, brightContours, i, cv::Scalar(0,127,127), 2);
                    
                    NSString *tempString = [NSString stringWithFormat:@"%@%@%@%@",
                                            (doesIntersect?@"":@"I"),
                                            (isBiggest?@"":@"B"),
                                            (isOverFinger?@"":@"F"),
                                            (isNotTooWide?@"":@"W")
                                            ];
                    
                    cv::putText(displayImage, [tempString UTF8String], brightBoundCenter+cv::Point(1,1),  cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0,0,0));
                    cv::putText(displayImage, [tempString UTF8String], brightBoundCenter,  cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0,255,255));
                }
                
                bool isValid = (doesIntersect && isBiggest && isOverFinger && isNotTooWide);
                
                if (isValid) {
                    // This is a potential fingernail (or at least contains the nail)
                    biggestOverlappingIndex = i;
                    biggestOverlappingSize = polyArea;
                }
                
            }
            
            if (biggestOverlappingIndex >= 0) {
                // This is the one we think is (or at leaset includes) the nail
                cv::drawContours(steps.potentialNailContoursMat, brightContours, biggestOverlappingIndex, 255, CV_FILLED);
            }
            else {
//                statusMessage = [statusMessage stringByAppendingString:@"⛔️ Nail not found\n"];
                continueProcessing = false;
            }
            
        }
        
        // Take away anything we think to be skin
        steps.potentialNailContoursMat -= steps.redFingerImage;
        cv::threshold(steps.potentialNailContoursMat, steps.potentialNailContoursMat, 128, 255, cv::THRESH_BINARY); // kind of a hack for a mask
        
        // Erode/dilate to get rid on tentrils and specs
        int erosion_size = FBTweakValue(@"Nail Finding", @"Nail", @"Erode", 3, 1, 10);
        cv::Mat erosionElement = getStructuringElement( cv::MORPH_ELLIPSE,
                                                       cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                                       cv::Point( erosion_size, erosion_size ) );
        cv::erode( steps.potentialNailContoursMat, steps.potentialNailContoursMat, erosionElement );
        
        int dilation_size = FBTweakValue(@"Nail Finding", @"Nail", @"Dilate", 3, 1, 10);
        cv::Mat dilationElement = getStructuringElement(cv::MORPH_RECT,
                                                        cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                                        cv::Point( dilation_size, dilation_size ) );
        cv::dilate(steps.potentialNailContoursMat, steps.potentialNailContoursMat, dilationElement);
        
        // Now look for biggest contour remaining
        if (continueProcessing) {
            cv::Mat potentialNailMatCopy = steps.potentialNailContoursMat.clone();
            
            std::vector<std::vector<cv::Point> > nailContours;
            cv::findContours(potentialNailMatCopy, nailContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
            int biggestNailContourIndex = cvt::findBiggestContourIndex(nailContours);
            
            if (biggestNailContourIndex >= 0) {
                
                nailContour = nailContours[biggestNailContourIndex];
                
                // Get stats on our nail contour
                nailBoundingRect = cv::boundingRect(nailContour);
                double nailContourArea = cv::contourArea(nailContour);
                
                // Check for centeredness
                {
                    int centerOffset = ABS((nailBoundingRect.x + (nailBoundingRect.width/2)) -
                                           (fingerSkinBoundingRect.x + (fingerSkinBoundingRect.width/2)));
                    
                    int maxNailFingerOffset = FBTweakValue(@"Nail Finding", @"Nail", @"Offcenter tolerance", 20, 1, 100);
                    
                    if (centerOffset > maxNailFingerOffset) {
//                        statusMessage = [statusMessage stringByAppendingString:@"⚠️ Nail not centered on finger.\n"];
                    }
                }
                
                // Check for minimum size
                {
                    double screenArea = src.cols * src.rows;
                    int minimumNailSizePercent = FBTweakValue(@"Nail Finding", @"Nail", @"Minimum size %", 2, 1, 100);
                    
                    if (((nailContourArea * 100) / screenArea) < minimumNailSizePercent) {
//                        statusMessage = [statusMessage stringByAppendingString:@"⚠️ Nail is small.\n"];
                    }
                }
                
                // Check for absolute maximum size
                {
                    // If it's bigger than 75% of screen, something's wrong
                    if ((nailBoundingRect.width > (src.cols * 0.75)) ||
                        (nailBoundingRect.height > (src.rows * 0.75))) {
//                        statusMessage = [statusMessage stringByAppendingString:@"⛔️ Nail too big.\n"];
                        continueProcessing = false;
                    }
                }
                
                // Check for absolute maximum size
                {
                    // If it's as big as the finger, then we've screwed ip and this isn't the nail.
                    if ((nailBoundingRect.height > (fingerSkinBoundingRect.height-10))) {
//                        statusMessage = [statusMessage stringByAppendingString:@"⛔️ Nail too big (Matches finger).\n"];
                        continueProcessing = false;
                    }
                }
                
                // Pass out matrix with pixels set to white where we found nail
                nailMat = steps.potentialNailContoursMat;
                
                
                // Draw debug info
                if (FBTweakValue(@"Nail Finding", @"Display", @"Final Nail Contour", YES)) {
                    if (continueProcessing) {
                        cv::drawContours(displayImage, nailContours, biggestNailContourIndex, cv::Scalar(0,0,0), 4);
                        cv::drawContours(displayImage, nailContours, biggestNailContourIndex, cv::Scalar(255,255,255), 2);
                    }
                    else {
                        cv::drawContours(displayImage, nailContours, biggestNailContourIndex, cv::Scalar(0,0,0), 4);
                        cv::drawContours(displayImage, nailContours, biggestNailContourIndex, cv::Scalar(127,127,127), 2);
                    }
                    
                    //                            cv::rectangle(displayImage, nailBoundingRec + cv::Size(1,1), cv::Scalar(0,0,0));
                    //                            cv::rectangle(displayImage, nailBoundingRec, cv::Scalar(127,127,127));
                }
            }
        }
        
    }
    
    return continueProcessing && (nailBoundingRect.width > 0) && (nailBoundingRect.height > 0); // This is a hack. We need to eliminate this "continueProcessing" cruft.
    
}