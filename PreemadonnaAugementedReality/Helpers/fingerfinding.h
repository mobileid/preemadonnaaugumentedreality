//
//  fingerfinding.h
//  Nail Finding Tester
//
//  Created by Stan James on 6/30/15.
//

#ifndef __Nail_Finding_Tester__fingerfinding__
#define __Nail_Finding_Tester__fingerfinding__

#include <stdio.h>
#import <opencv2/opencv.hpp>

class fingerfinding {

public:
    
    //
    // Find red areas presumed to be skin
    //
    static void findRedAreas(double redThreshold, cv::Mat src, cv::Mat& dst, cv::Mat& displayImage);

    //
    // Find fingernail
    //
    struct findFingerNailSteps {
        cv::Mat potentialNailPixelsMat;
        cv::Mat redFingerImage;
        cv::Mat potentialNailContoursMat;
        cv::Mat grayImage;
    };
    static bool findFingerNail(cv::Mat src, cv::Mat& nailMat, std::vector<cv::Point>& nailContour, cv::Rect& nailBoundingRect, cv::Mat& displayImage);
    static bool findFingerNail(cv::Mat src, cv::Mat& nailMat, std::vector<cv::Point>& nailContour, cv::Rect& nailBoundingRect, cv::Mat& displayImage, findFingerNailSteps& steps);

};

class findFingerNail {
    
    findFingerNail(cv::Mat src);
};
#endif /* defined(__Nail_Finding_Tester__fingerfinding__) */
