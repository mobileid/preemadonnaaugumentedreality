//
//  cvtoolbox.h
//  Nail Finding Tester
//
//  Created by Stan James on 6/29/15.
//

#ifndef __Nail_Finding_Tester__cvtoolbox__
#define __Nail_Finding_Tester__cvtoolbox__

#include <stdio.h>
#import <opencv2/opencv.hpp>

int colorlessThreshold();

class cvt {
    
public:
    static int findBiggestContourIndex(std::vector<std::vector<cv::Point> > contours);
    
};

#endif /* defined(__Nail_Finding_Tester__cvtoolbox__) */
