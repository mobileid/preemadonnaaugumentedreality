//
//  cvtoolbox.cpp
//  Nail Finding Tester
//
//  Created by Stan James on 6/29/15.
//

#include "cvtoolbox.h"

void colorSaturation (cv::Mat img, double thresh) {
    uchar r, g, b;
    for (int i = 0; i < img.rows; ++i)
    {
        cv::Vec3b* pixel = img.ptr<cv::Vec3b>(i); // point to first pixel in row
        for (int j = 0; j < img.cols; ++j)
        {
            r = pixel[j][2];
            g = pixel[j][1];
            b = pixel[j][0];
        }
    }
    
}

/**
 * Small helper function to get biggest contour
 */
int cvt::findBiggestContourIndex(std::vector<std::vector<cv::Point> > contours)
{
    int biggestContourIndex = -1;
    double biggestContourSize = 0.0;
    for( size_t i = 0; i < contours.size(); i++ ) {
        double polyArea = cv::contourArea(contours[i]);
        if (polyArea > biggestContourSize) {
            biggestContourIndex = i;
            biggestContourSize = polyArea;
        }
    }
    return biggestContourIndex;
}
