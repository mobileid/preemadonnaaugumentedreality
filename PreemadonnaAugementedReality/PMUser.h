//
//  PMUser.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 19/10/15.
//  Copyright © 2015 LeftRightMind. All rights reserved.
//

#import "PFUser.h"
#import <Parse/Parse.h>

@interface PMUser : PFUser
+(void)logInForPreemadonnaInBackgroundWithUsername:(NSString*) username password:(NSString *) password withBlock:(void(^)( PFUser * user,NSError * error))block;
-(void)signInForPreemadonnaInBackgroundWithUser:(PFUser *) user withBlock:(void(^)(BOOL status, NSError * error ))block;
-(void)savePreemadonnaUserInBackgroundWithBlock:(void(^)(BOOL status, NSError * error ))block;

@end
