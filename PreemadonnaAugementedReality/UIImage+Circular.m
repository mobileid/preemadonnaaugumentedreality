//
//  UIImage+Circular.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 05/10/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "UIImage+Circular.h"
#import "Preemadonna.h"
@interface UIImage (Circular)

@end
@implementation UIImage (Circular)
- (UIImage*)circularScaleAndCropImage:(UIImage*)image frame:(CGRect)frame {
    
    // This function returns a newImage, based on image, that has been:
    // - scaled to fit in (CGRect) rect
    // - and cropped within a circle of radius: rectWidth/2
    [Preemadonna sharedClient].defaultImage = image;
    //Create the bitmap graphics context
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(frame.size.width, frame.size.height), NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Get the width and heights
    CGFloat imageWidth = image.size.width;
    CGFloat imageHeight = image.size.height;
    CGFloat rectWidth = frame.size.width;
    CGFloat rectHeight = frame.size.height;
    
    //Calculate the scale factor
    CGFloat scaleFactorX = rectWidth/imageWidth;
    CGFloat scaleFactorY = rectHeight/imageHeight;
    
    //Calculate the centre of the circle
    CGFloat imageCentreX = rectWidth/2;
    CGFloat imageCentreY = rectHeight/2;
    
    // (This could be replaced with any closed path if you want a different shaped clip)
    CGFloat radius = rectWidth/2;
    CGContextBeginPath (context);
    CGContextAddArc (context, imageCentreX, imageCentreY, radius, 0, 2*M_PI, 0);
    CGContextClosePath (context);
    CGContextClip (context);
    
    //Set the SCALE factor for the graphics context
    //All future draw calls will be scaled by this factor
    CGContextScaleCTM (context, scaleFactorX, scaleFactorY);
    
    // Draw the IMAGE
    CGRect myRect = CGRectMake(0, 0, imageWidth, imageHeight);
    [image drawInRect:myRect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (void)drawRect:(CGRect)rect {
    
    CGFloat oImageWidth = [Preemadonna sharedClient].defaultImage.size.width;
    CGFloat oImageHeight = [Preemadonna sharedClient].defaultImage.size.height;
    // Draw the original image at the origin
    CGRect oRect = CGRectMake(0, 0, oImageWidth, oImageHeight);
    [[Preemadonna sharedClient].defaultImage drawInRect:oRect];
    
    // Set the newRect to half the size of the original image
    CGRect newRect = CGRectMake(0, 0, oImageWidth/2, oImageHeight/2);
    UIImage *newImage = [self circularScaleAndCropImage:[Preemadonna sharedClient].defaultImage frame:newRect];
    
    CGFloat nImageWidth = newImage.size.width;
    CGFloat nImageHeight = newImage.size.height;
    
    //Draw the scaled and cropped image
    CGRect thisRect = CGRectMake(oImageWidth+10, 0, nImageWidth, nImageHeight);
    [newImage drawInRect:thisRect];
    
}

@end
