//
//  AppDelegate.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 07/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//
#import "AppDelegate.h"
#import <Tweaks/FBTweak.h>
#import <Tweaks/FBTweakShakeWindow.h>
#import <Tweaks/FBTweakInline.h>
#import <Tweaks/FBTweakViewController.h>
#import <Parse/Parse.h>
#import "Preemadonna.h"
#import "PreemadonnaViewController.h"
#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIFields.h>
#import "GAI.h"
#import "GAITrackedViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    // Landing Page Last longer
    [NSThread sleepForTimeInterval:5];
    
    //Parse Login not needed
    [Parse setApplicationId:@"3Cn8qZoPHsJsOTZ0CqoAUbI0RRLki3IVewVXpy39" clientKey:@"63AW5upQzp8BPIPeOSuThV5eRV2fO2pIqTzcp4zU"];
    PFACL * acl = [[PFACL alloc] init];
    [acl setPublicReadAccess:YES];
    [acl setPublicWriteAccess:YES];
    [PFACL setDefaultACL:acl withAccessForCurrentUser:YES];
    [Preemadonna sharedClient].curruntUser = [PFUser currentUser];
    //[self setLogin];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    
    //Google Analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 1 ;
    // Create tracker instance.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-42603249-4"];
    
    //Session
    [[[GAI sharedInstance] defaultTracker] set:kGAISessionControl value:@"start"];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-42603249-4"];
    [tracker set:kGAISessionControl value:@"end"];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (UIWindow *)window
{
    if (!_window) {
        _window = [[FBTweakShakeWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    }
    
    return _window;
}

-(void)setLogin{
    
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    if ([Preemadonna sharedClient].curruntUser) {
        UINavigationController * homeNavController = [storyBoard instantiateViewControllerWithIdentifier:@"NavHomeViewController"];
        self.window.rootViewController = homeNavController;
    }else
    {
        UINavigationController * preemadonnaNavController = [storyBoard instantiateViewControllerWithIdentifier:@"NavPreemadonnaViewController"];
          self.window.rootViewController= preemadonnaNavController;

    }
}

@end
