//
//  nailFindingViewController.m
//
//
//

#import "nailFindingViewController.h"
#import "UIImage+OpenCV.h"

#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AudioToolbox/AudioToolbox.h>

#import <Tweaks/FBTweak.h>
#import <Tweaks/FBTweakInline.h>

#import "fingerfinding.h"
#import "cvtoolbox.h"

#import <MessageUI/MessageUI.h>
#import "ShareViewController.h"
@interface nailFindingViewController(){
    cv::Mat selectedNailImageMat;
    
}
@end

@implementation nailFindingViewController {
    dispatch_queue_t _processImageQueue;
    dispatch_semaphore_t _processImageBusySemaphore;
    
    cv::Rect _viewPortRect; // What we will show the user. Dampened movement/zoom.
    
    cv::Rect _nailRect; // Where the nail is. Dampened to move only so much over time.
    int _framesSinceLastNail; // How many frames since we found a nail? (Used to dampen flicker)
    
}

- (void) viewDidLoad
{
    self.navigationItem.hidesBackButton = YES;
    
    UIImage *chosenImage = [self stripUIImageTransparency:self.defaultNailUIImage];
    
    cv::Mat tempMat = [chosenImage CVMat];
    cv::Mat tempMatBGR;
    cv::cvtColor(tempMat, tempMatBGR, CV_BGRA2BGR);
    
    cv::flip(tempMatBGR, tempMatBGR, -1); // rotate 180
    
    selectedNailImageMat = tempMatBGR.clone();
    
    // Setup image processing queue + semaphore
    _processImageQueue = dispatch_queue_create("com.playground.sampleQueue", DISPATCH_QUEUE_SERIAL);
    _processImageBusySemaphore = dispatch_semaphore_create(1);
    
    // Start video
    {
        self.videoCamera = [[CvVideoCamera alloc] init];
        // See other defaults here: http://fossies.org/dox/opencv-2.4.10/cap__ios__abstract__camera_8mm_source.html
        self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
        self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationLandscapeRight;
        self.videoCamera.defaultFPS = 30;
        self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPresetiFrame960x540; //AVCaptureSessionPreset1280x720;
        //        self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
        //        AVCaptureSessionPreset352x288
        self.videoCamera.delegate = self;
        
    }

    // Image picker stuff
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"No Camera"
                                                              message:@"This device has no camera, so fingernail finding cannot function."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    
    // Get default image
    {
        UIImage *defaultNailUIImageRaw = self.defaultNailUIImage;
        UIImage *defaultNailUIImage = [self stripUIImageTransparency:defaultNailUIImageRaw];
        cv::cvtColor([defaultNailUIImage CVMat], selectedNailImageMat, CV_BGRA2BGR);
    }
    
    _viewPortRect = cv::Rect(0,0,0,0);
    _nailRect = cv::Rect(0,0,0,0);
    _framesSinceLastNail = 0;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
//MARK:- Camera Starting modified
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.videoCamera start];
}
//MARK:- Turning off Flash
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            
            [device lockForConfiguration:nil];
            if (device.torchMode == AVCaptureTorchModeOn)
            {
                [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
            }
            [device unlockForConfiguration];
        }
    }
    [self.videoCamera stop];
    
}

- (UIImage*) stripUIImageTransparency:(UIImage*)image
{
    // check if there is alpha channel
    CGImageAlphaInfo alpha = CGImageGetAlphaInfo(image.CGImage);
    if (alpha == kCGImageAlphaPremultipliedLast || alpha == kCGImageAlphaPremultipliedFirst ||
        alpha == kCGImageAlphaLast || alpha == kCGImageAlphaFirst || alpha == kCGImageAlphaOnly)
    {
        // create the context with information from the original image
        CGContextRef bitmapContext = CGBitmapContextCreate(NULL,
                                                           image.size.width,
                                                           image.size.height,
                                                           CGImageGetBitsPerComponent(image.CGImage),
                                                           CGImageGetBytesPerRow(image.CGImage),
                                                           CGImageGetColorSpace(image.CGImage),
                                                           CGImageGetBitmapInfo(image.CGImage)
                                                           );
        
        // draw white rect as background
        CGContextSetFillColorWithColor(bitmapContext, [UIColor whiteColor].CGColor);
        CGContextFillRect(bitmapContext, CGRectMake(0, 0, image.size.width, image.size.height));
        
        // draw the image
        CGContextDrawImage(bitmapContext, CGRectMake(0, 0, image.size.width, image.size.height), image.CGImage);
        CGImageRef resultNoTransparency = CGBitmapContextCreateImage(bitmapContext);
        
        // get the image back
        image = [UIImage imageWithCGImage:resultNoTransparency];
        
        // do not forget to release..
        CGImageRelease(resultNoTransparency);
        CGContextRelease(bitmapContext);
    }
    return image;
}

//MARK:- Flash toggle modified
- (IBAction) toggleFlash:(id)sender {
    // check if flashlight available
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            
            [device lockForConfiguration:nil];
            if (device.torchMode == AVCaptureTorchModeOff)
            {
                [device setTorchMode:AVCaptureTorchModeOn];
                [device setFlashMode:AVCaptureFlashModeOn];
                [(UIButton*)sender setImage:[UIImage imageNamed:@"flashoff"] forState:UIControlStateNormal];
                //torchIsOn = YES;
            }
            else
            {
                [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
                [(UIButton*)sender setImage:[UIImage imageNamed:@"flash"] forState:UIControlStateNormal];
                // torchIsOn = NO;
            }
            [device unlockForConfiguration];
        }
    }
}

#pragma mark ** Process Image ***

/**
 * Extract our region of interest from the raw video feed
 */
- (cv::Mat) getROIofImage:(cv::Mat)image
{
    int reticleScalePercent = FBTweakValue(@"General", @"Reticle", @"Size % of width", 100, 1, 100);
    int reticleDimensions = image.cols * reticleScalePercent;
    BOOL doReticle = FBTweakValue(@"General", @"Reticle", @"Use Reticle?", YES);
    cv::Mat reticleImage;
    
    // Prevent ROI from being bigger than image
    reticleDimensions = std::min(std::min(reticleDimensions, image.cols), image.rows);
    
    if (doReticle) {
        cv::Size reticleSize = cv::Size(reticleDimensions, reticleDimensions);
        
        cv::Point reticleOrigin = (FBTweakValue(@"General", @"Reticle", @"Reticle South", NO) ?
                                   cv::Point(image.cols/2 - reticleSize.width/2, image.rows - reticleSize.height) :
                                   cv::Point(image.cols/2 - reticleSize.width/2, image.rows/2 - reticleSize.height/2));
        
        cv::Rect reticleRect = cv::Rect (reticleOrigin, reticleSize);
        reticleImage = cv::Mat::ones(reticleSize, CV_8UC3);
        
        // Extract reticle part of image
        // Convert to BGR
        // Copy over portion we care about, stripping transparancy. This could probably be optimized to avoid the copy
        cv::cvtColor(image(reticleRect), reticleImage, CV_RGBA2BGR);
    }
    else {
        // Use entire image as "the reticle"
        cv::cvtColor(image, reticleImage, CV_RGBA2BGR);
    }
    
    return reticleImage;
}

- (void) processImage:(cv::Mat &)rawImageFullSize
{
    // See if we're already busy
    if (dispatch_semaphore_wait(_processImageBusySemaphore, DISPATCH_TIME_NOW) != 0) {
        return;
    }
    
    // Check our orientation
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft) {
        // Rotate our image 180
        cv::flip(rawImageFullSize, rawImageFullSize, -1); // rotate 180
    }
    
    cv::Mat imageFullSize = [self getROIofImage:rawImageFullSize];
    cv::Mat image;
    cv::pyrDown(imageFullSize, image);
    
    dispatch_async(_processImageQueue, ^{
        
        double maxFingerRoughness = FBTweakValue(@"Nail Finding", @"Finger", @"maxFingerRoughness", 1.2, 0.5, 4.00);
        
        // for drawing debugging crap onto
        cv::Mat displayImage = cv::Mat::zeros(image.size(), image.type());
        
        NSString* statusMessage = @"";
        
        // DO STUFF
        cv::Mat fingerThresholdImage;
        cv::Mat redFingerImage;
        cv::Mat potentialNailMat;
        std::vector<cv::Point> nailContour;
        cv::Rect nailBoundingRec = cv::Rect(0,0,0,0); // rectangle around the nail
        cv::Mat croppedNailImageMat; // raw image cropped to just the nail
        cv::Mat viewPortImageMat; // our zoomed/cropped view
        
        //
        // Find finger and nail
        //
        {
            cv::Mat grayImage;
            cv::cvtColor(image, grayImage, CV_BGRA2GRAY);
            bool continueProcessing = true;
            
            // Find the best finger contour
            double meanFingerBrightness = 0.0;
            double fingerSkinContourRoughness = 100.0;
            std::vector<cv::Point> fingerSkinContour;
            cv::Rect fingerSkinBoundingRect = cv::Rect(0,0,0,0);
            int fingerRedThreshold;
            
            for (fingerRedThreshold = 4; fingerRedThreshold < 100; fingerRedThreshold += 4) {
                
                fingerSkinContour.clear();
                
                // Find redish (skin-ish) areas of image
                fingerfinding::findRedAreas(fingerRedThreshold, image, redFingerImage, displayImage);
                
                meanFingerBrightness = cv::mean(grayImage, redFingerImage)[0];
                
                
                // Find biggest contour of skin
                std::vector<std::vector<cv::Point> > fingerSkinContours;
                int biggestContourIndex = -1;
                if (continueProcessing) {
                    cv::Mat redFingerCopy = redFingerImage.clone();
                    cv::findContours(redFingerCopy, fingerSkinContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
                    
                    biggestContourIndex = cvt::findBiggestContourIndex(fingerSkinContours);
                    
                    if (biggestContourIndex >= 0) {
                        fingerSkinContour = fingerSkinContours[biggestContourIndex];
                        fingerSkinBoundingRect = cv::boundingRect(fingerSkinContour);
                    }
                    else {
                        statusMessage = [statusMessage stringByAppendingString:@"⛔️ Finger not found\n"];
                        continueProcessing = false;
                    }
                }
                
                if ((fingerSkinContour.size() > 0) && biggestContourIndex) {
                    // Evaluate for convexity
                    std::vector<cv::Point> hull;
                    cv::convexHull(fingerSkinContour, hull);
                    
                    double rawPerimeter = cv::arcLength(fingerSkinContour, true);
                    double hullPerimeter = cv::arcLength(hull, true);
                    fingerSkinContourRoughness = rawPerimeter / hullPerimeter;
                    
                    // Show it
                    
                    // See if our "finger" extends the whole width/height of image
                    // (with 5 pixel fudge factor)
                    bool isTooBig = ((fingerSkinBoundingRect.size().width  >= (image.cols - 5)) ||
                                     (fingerSkinBoundingRect.size().height >= (image.rows - 5)));
                    
                    bool isTooSmall = ((fingerSkinBoundingRect.area()*100 / image.size().area()) < 10); // require min of 10% of image be finger
                    
                    bool isSmoothEngough = (fingerSkinContourRoughness < maxFingerRoughness);
                    
                    if (isSmoothEngough && !isTooBig && !isTooSmall) {
                        if (FBTweakValue(@"Display", @"Layers", @"Finger Contour", YES)) {
                            cv::drawContours(displayImage, fingerSkinContours, biggestContourIndex, cv::Scalar(255,0,0), 2);
                        }
                        
                        // stop our searching
                        break;
                    }
                    
                }
                
            }
            
            //
            // Find pixels likely to belong to the nail
            //
            if (continueProcessing) {
                
                // Look for color-less areas of the image (i.e. white/gray/black)
                // DEBUG
                cv::Mat imageHSV;
                cv::cvtColor(image, imageHSV, CV_BGR2HSV);
                cv::Mat channels[3];
                cv::split(imageHSV, channels);
                cv::Mat saturationMat = channels[1];
                // A pixel can have a saturation of AT MOST this much to be included as possible nail
                int saturationThreshold = FBTweakValue(@"Nail Finding", @"Nail", @"Max Saturation", 15, 0, 255);
                cv::threshold(saturationMat, saturationMat, saturationThreshold, 255, CV_THRESH_BINARY_INV);
                // At this point, white pixels indicate potential nail areas (based on saturation)
                
                // Find pixels that are at least as *bright* as finger ... this should include the nail
                cv::Mat brightMat;
                int brightnessThreshholdShift = FBTweakValue(@"Nail Finding", @"Nail", @"Brightness thresh shift", -30, -100, +100);
                double fingerLightThreshold = MAX(MIN(meanFingerBrightness + brightnessThreshholdShift, 255), 0); // Must be at leaset as bright as the finger itself
                cv::threshold(grayImage, brightMat, fingerLightThreshold, 255, cv::THRESH_BINARY);
                
                cv::bitwise_and(brightMat, saturationMat, fingerThresholdImage);
                
                // might need to add an erode step (but only if contour finding is slowing us down too much)
                int dilation_size = FBTweakValue(@"Nail Finding", @"Finger", @"Dilate", 3, 1, 10);
                cv::Mat dilationElement = getStructuringElement(cv::MORPH_RECT,
                                                                cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                                                cv::Point( dilation_size, dilation_size ) );
                cv::dilate(fingerThresholdImage, fingerThresholdImage, dilationElement);
                
                // Debug display
                if (FBTweakValue(@"Display", @"Layers", @"finger threshold", NO)) {
                    displayImage.setTo(cv::Scalar(127,0,0), fingerThresholdImage);
                }
                // Debug display
                if (FBTweakValue(@"Display", @"Layers", @"finger bright", NO)) {
                    displayImage.setTo(cv::Scalar(0,127,0), brightMat);
                }
                // Debug display
                if (FBTweakValue(@"Display", @"Layers", @"finger saturation", NO)) {
                    displayImage.setTo(cv::Scalar(0,0,127), saturationMat);
                }
                
            }
            
            // Find bright contours that overlap with finger -- this should include the nail
            // (and exclude noise)
            if (continueProcessing) {
                potentialNailMat = cv::Mat::zeros(image.size(), CV_8UC1);
                
                // Find contours in our mat of bright, unsaturated areas
                std::vector<std::vector<cv::Point> > brightContours;
                if (fingerSkinContour.size() > 0) {
                    cv::Mat fingerThresholdImageCopy = fingerThresholdImage.clone();
                    cv::findContours(fingerThresholdImageCopy, brightContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
                    
                    int biggestOverlappingIndex = -1;
                    double biggestOverlappingSize = 0;
                    
                    for( size_t i = 0; i < brightContours.size(); i++ ) {
                        // for now we use simple bounding boxes. we could get fancier and test for overlap of the actual
                        // pixels in the contours.
                        cv::Rect brightBoundRect = cv::boundingRect(brightContours[i]);
                        
                        cv::Point brightBoundCenter = cv::Point(brightBoundRect.x + (brightBoundRect.width/2),
                                                                brightBoundRect.y + (brightBoundRect.height/2));
                        
                        // Skip contours obivously too big
                        //                        if (brightBoundRect.width > (image.cols / 2)) {
                        //                            continue;
                        //                        }
                        
                        // See: http://opencv.itseez.com/modules/core/doc/basic_structures.html?highlight=rect#Rect_
                        cv::Rect intersection = (brightBoundRect & fingerSkinBoundingRect);
                        double polyArea = cv::contourArea(brightContours[i]);
                        
                        // Does contour intersect our finger region?
                        bool doesIntersect = (intersection.width > 0);
                        // Is it bigger than any valid contour we've seen so far?
                        bool isBiggest = (polyArea > biggestOverlappingSize);
                        // Does it touch the bottom of the image? (means likely to be noise) -- This had too many false positives
                        //bool touchesBottom = (brightBoundRect.y+brightBoundRect.height) >= (image.rows - 5);
                        // Does the the x centerpoint of our nail lie within widht of our finger? (i.e. not hanging off the side?)
                        bool isOverFinger = (brightBoundCenter.x > fingerSkinBoundingRect.x) && (brightBoundCenter.x < (fingerSkinBoundingRect.x+fingerSkinBoundingRect.width));
                        // Is the nail rectangle smaller that some percent of our finger?
                        bool isNotTooWide = (brightBoundRect.width < (fingerSkinBoundingRect.width * 1.2));
                        
                        bool doBrightContourDebugDisplay = (FBTweakValue(@"Display", @"Layers", @"Bright contours", NO));
                        
                        
                        // Debug display
                        if (doBrightContourDebugDisplay) {
                            cv::drawContours(displayImage, brightContours, i, cv::Scalar(0,0,0), 4);
                            cv::drawContours(displayImage, brightContours, i, cv::Scalar(0,127,127), 2);
                            
                            NSString *tempString = [NSString stringWithFormat:@"%@%@%@%@",
                                                    (doesIntersect?@"":@"I"),
                                                    (isBiggest?@"":@"B"),
                                                    (isOverFinger?@"":@"F"),
                                                    (isNotTooWide?@"":@"W")
                                                    ];
                            
                            cv::putText(displayImage, [tempString UTF8String], brightBoundCenter+cv::Point(1,1),  cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0,0,0));
                            cv::putText(displayImage, [tempString UTF8String], brightBoundCenter,  cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0,255,255));
                        }
                        
                        bool isValid = (doesIntersect && isBiggest && isOverFinger && isNotTooWide);
                        
                        if (isValid) {
                            // This is a potential fingernail (or at least contains the nail)
                            biggestOverlappingIndex = i;
                            biggestOverlappingSize = polyArea;
                        }
                    }
                    
                    if (biggestOverlappingIndex >= 0) {
                        // This is the one we think is (or at leaset includes) the nail
                        cv::drawContours(potentialNailMat, brightContours, biggestOverlappingIndex, 255, CV_FILLED);
                    }
                    else {
                        statusMessage = [statusMessage stringByAppendingString:@"⛔️ Nail not found\n"];
                        continueProcessing = false;
                    }
                    
                }
                
                // Take away anything we think to be skin
                potentialNailMat -= redFingerImage;
                cv::threshold(potentialNailMat, potentialNailMat, 128, 255, cv::THRESH_BINARY); // kind of a hack for a mask
                
                // Erode/dilate to get rid on tentrils and specs
                int erosion_size = FBTweakValue(@"Nail Finding", @"Nail", @"Erode", 3, 1, 10);
                cv::Mat erosionElement = getStructuringElement( cv::MORPH_ELLIPSE,
                                                               cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                                               cv::Point( erosion_size, erosion_size ) );
                cv::erode( potentialNailMat, potentialNailMat, erosionElement );
                
                int dilation_size = FBTweakValue(@"Nail Finding", @"Nail", @"Dilate", 3, 1, 10);
                cv::Mat dilationElement = getStructuringElement(cv::MORPH_RECT,
                                                                cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                                                cv::Point( dilation_size, dilation_size ) );
                cv::dilate(potentialNailMat, potentialNailMat, dilationElement);
                
                // Now look for biggest contour remaining
                if (continueProcessing) {
                    cv::Mat potentialNailMatCopy = potentialNailMat.clone();
                    
                    std::vector<std::vector<cv::Point> > nailContours;
                    cv::findContours(potentialNailMatCopy, nailContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
                    int biggestNailContourIndex = cvt::findBiggestContourIndex(nailContours);
                    
                    if (biggestNailContourIndex >= 0) {
                        
                        nailContour = nailContours[biggestNailContourIndex];
                        
                        // Get stats on our nail contour
                        nailBoundingRec = cv::boundingRect(nailContour);
                        double nailContourArea = cv::contourArea(nailContour);
                        
                        // Check for centeredness
                        {
                            int centerOffset = ABS((nailBoundingRec.x + (nailBoundingRec.width/2)) -
                                                   (fingerSkinBoundingRect.x + (fingerSkinBoundingRect.width/2)));
                            
                            int maxNailFingerOffset = FBTweakValue(@"Nail Finding", @"Nail", @"Offcenter tolerance", 20, 1, 100);
                            
                            if (centerOffset > maxNailFingerOffset) {
                                statusMessage = [statusMessage stringByAppendingString:@"⚠️ Nail not centered on finger.\n"];
                            }
                        }
                        
                        // Check for minimum size
                        {
                            double screenArea = image.cols * image.rows;
                            int minimumNailSizePercent = FBTweakValue(@"Nail Finding", @"Nail", @"Minimum size %", 2, 1, 100);
                            
                            if (((nailContourArea * 100) / screenArea) < minimumNailSizePercent) {
                                statusMessage = [statusMessage stringByAppendingString:@"⚠️ Nail is small.\n"];
                            }
                        }
                        
                        // Check for absolute maximum size
                        {
                            // If it's bigger than 75% of screen, something's wrong
                            if ((nailBoundingRec.width > (image.cols * 0.75)) ||
                                (nailBoundingRec.height > (image.rows * 0.75))) {
                                statusMessage = [statusMessage stringByAppendingString:@"⛔️ Nail too big.\n"];
                                continueProcessing = false;
                            }
                        }
                        
                        // Check for absolute maximum size
                        {
                            // If it's as big as the finger, then we've screwed ip and this isn't the nail.
                            if ((nailBoundingRec.height > (fingerSkinBoundingRect.height-10))) {
                                statusMessage = [statusMessage stringByAppendingString:@"⛔️ Nail too big (Matches finger).\n"];
                                continueProcessing = false;
                            }
                        }
                        
                        //                        // Check concavity
                        //                        {
                        //                            BOOL isConvex = cv::isContourConvex(nailContour);
                        //                            if (!isConvex) {
                        //                                statusMessage = [statusMessage stringByAppendingString:@"⛔️ Nail is concave.\n"];
                        //                            }
                        //                        }
                        
                        // Draw debug info
                        if (FBTweakValue(@"Display", @"Layers", @"Final Nail Contour", YES)) {
                            if (continueProcessing) {
                                cv::drawContours(displayImage, nailContours, biggestNailContourIndex, cv::Scalar(0,0,0), 4);
                                cv::drawContours(displayImage, nailContours, biggestNailContourIndex, cv::Scalar(255,255,255), 2);
                            }
                            else {
                                cv::drawContours(displayImage, nailContours, biggestNailContourIndex, cv::Scalar(0,0,0), 4);
                                cv::drawContours(displayImage, nailContours, biggestNailContourIndex, cv::Scalar(127,127,127), 2);
                            }
                            
                            //                            cv::rectangle(displayImage, nailBoundingRec + cv::Size(1,1), cv::Scalar(0,0,0));
                            //                            cv::rectangle(displayImage, nailBoundingRec, cv::Scalar(127,127,127));
                        }
                    }
                }
            }
            
            
        } // end of finger & nail finding
        
        
        // Dampen flickering of our nail
        int nailRectCacheFramecount = FBTweakValue(@"Display", @"Viewport", @"Nail Flicker Cache Count", 5, 0, 30);
        
        if (nailBoundingRec.width > 0) {
            // We have a nail .. reset our counter
            _framesSinceLastNail = 0;
            // Remember where we found the nail
            _nailRect = nailBoundingRec;
        }
        else {
            // No nail
            _framesSinceLastNail++;
            
            if ((_framesSinceLastNail < nailRectCacheFramecount) &&
                (_nailRect.width > 0))
            {
                nailBoundingRec = _nailRect;
                statusMessage = [statusMessage stringByAppendingString:@"⚠️ Using old nail contour\n"];
            }
        }
        
        
        // Overlay our nail image, if we have one
        if (selectedNailImageMat.data && ((nailBoundingRec.width > 0))) {
            
            cv::Mat resizedNailImage;
            cv::Mat resizedNailImageFullSize;
            cv::Rect nailImageDestRect;
            cv::Rect nailImageDestRectFullSize;
            if (FBTweakValue(@"Display", @"Nail", @"Fill entire nail", NO)) {
                // Have nail image fill entire nail (stretches to fit)
                nailImageDestRect = nailBoundingRec;
            }
            else {
                // Center nail image in nail, keeping aspect ratio
                
                float nailImageSizePercent = FBTweakValue(@"Display", @"Nail", @"Image size %", .70, .10, 1.0);
                
                // Find rect on nail at proportions of image
                int newWidth = (nailBoundingRec.width * nailImageSizePercent);
                int newHeight = (((selectedNailImageMat.rows * nailBoundingRec.width) / selectedNailImageMat.cols) * nailImageSizePercent);
                nailImageDestRect = cv::Rect(nailBoundingRec.x,
                                             nailBoundingRec.y,
                                             newWidth,
                                             newHeight
                                             );
                //                // Add padding around (reduce image a little.)
                //                cv::Size deltaSize(nailImageDestRect.width * nailImageSizePercent,
                //                                   nailImageDestRect.height * nailImageSizePercent);
                //                nailImageDestRect += deltaSize;
                
                cv::Point offset((nailBoundingRec.width - newWidth)/2,
                                 (nailBoundingRec.height - newHeight)/2);
                nailImageDestRect += offset;
            }
            
            //            // Reduced scale debug view of nail image
            //            {
            //                // Create version of nail image at half scale
            //                cv::resize(self.selectedNailImageMat, resizedNailImage, nailImageDestRect.size());
            //                // Keep our rect in bounds of image
            //                nailImageDestRect = nailImageDestRect & cv::Rect(cv::Point(0,0), image.size());
            //                // Create our mask
            //                cv::Mat nailMask = cv::Mat::zeros(image.size(), CV_8UC1);
            //                potentialNailMat(nailImageDestRect).copyTo(nailMask);
            //                // Copy to debug view
            //                resizedNailImage.copyTo(displayImage(nailImageDestRect), nailMask);
            //            }
            
            // Full-scale nail image
            {
                nailImageDestRectFullSize = cv::Rect(
                                                     2 * nailImageDestRect.x,
                                                     2 * nailImageDestRect.y,
                                                     2 * nailImageDestRect.width,
                                                     2 * nailImageDestRect.height);
                // Keep our rect in bounds of image
                nailImageDestRectFullSize = nailImageDestRectFullSize & cv::Rect(cv::Point(0,0), imageFullSize.size());
                
                // Create version of nail image at full resolution
                cv::resize(selectedNailImageMat, resizedNailImageFullSize, nailImageDestRectFullSize.size(), CV_INTER_CUBIC);
                
                // Adjust brightness to match nail
                if (FBTweakValue(@"Display", @"Nail", @"Image matches nail brightness", YES)) {
                    cv::Mat resizedNailImageFullSize32;
                    resizedNailImageFullSize.convertTo(resizedNailImageFullSize32, CV_32FC3, 1.0/255.0);
                    cv::Mat whiteScaleForNail32;
                    imageFullSize(nailImageDestRectFullSize).convertTo(whiteScaleForNail32, CV_32FC3, 1.0/255.0);
                    // Multiplying by our white (in range 0-1) makes our brightness the same
                    cv::multiply(resizedNailImageFullSize32, whiteScaleForNail32, resizedNailImageFullSize32);
                    resizedNailImageFullSize32.convertTo(resizedNailImageFullSize, CV_8UC3, 255.0/1.0);
                }
                
                // Copy to camera view * NO MASK
                resizedNailImageFullSize(cv::Rect(cv::Point(0,0), nailImageDestRectFullSize.size()))
                .copyTo(imageFullSize(nailImageDestRectFullSize));
            }
            
        }
        
        // Create cropped view of nail for our viewport
        cv::Rect targetViewportRect;
        if (nailBoundingRec.width > 0)
        {
            targetViewportRect = nailBoundingRec;
            
            // A multiplier to make the nail rectangle taller,
            // to show more of the finger below the nail
            float vertCenteringPercent = FBTweakValue(@"Display", @"Viewport", @"Vertical Centering", 1.5, 0.5, 3.00);
            targetViewportRect.height *= vertCenteringPercent;
            
            // Force aspect ratio to square
            targetViewportRect.width = targetViewportRect.height;
            
            // Expand a little to give padding
            float paddingPercent = FBTweakValue(@"Display", @"Viewport", @"Zoom padding %", 1.5, 0.0, 3.00);
            
            cv::Size deltaSize(targetViewportRect.width * paddingPercent,
                               targetViewportRect.height * paddingPercent);
            targetViewportRect += deltaSize;
            
            cv::Point offset(deltaSize.width/2, deltaSize.height/2);
            targetViewportRect -= offset;
            
            // keep in bounds
            targetViewportRect = targetViewportRect & cv::Rect(cv::Point(0,0), displayImage.size());
            
            //            // keep in bounds
            //            if ((nailBoundingRec.width + nailBoundingRec.x) > image.cols) {
            //                nailBoundingRec.x = image.cols - nailBoundingRec.width;
            //            }
            //            if ((nailBoundingRec.height + nailBoundingRec.y) > image.rows) {
            //                nailBoundingRec.y = image.rows - nailBoundingRec.height;
            //            }
            //            nailBoundingRec.x = MAX(nailBoundingRec.x, 0);
            //            nailBoundingRec.y = MAX(nailBoundingRec.y, 0);
            
            croppedNailImageMat = image(targetViewportRect).clone();
        }
        else {
            // No nail found, so we zoom to show whole image
            targetViewportRect = cv::Rect(cv::Point(0,0), displayImage.size());
        }
        
        // Move our viewport
        {
            if ((_viewPortRect.width == 0) &&
                (_viewPortRect.height == 0 ))
            {
                // Reset to whole screen
                _viewPortRect = cv::Rect(0, 0, image.cols, image.rows);
            }
            
            // Zoom to our nail
            float zoomRate = FBTweakValue(@"Display", @"Viewport", @"zoom rate %", 0.10, 0.01, 1.00);
            
            _viewPortRect += (targetViewportRect.tl() - _viewPortRect.tl()) * zoomRate;
            
            cv::Size deltaSize((targetViewportRect.width - _viewPortRect.width) * zoomRate,
                               (targetViewportRect.height - _viewPortRect.height) * zoomRate);
            _viewPortRect += deltaSize;
            
            //            _viewPortRect.width  = targetRect.width;
            //            _viewPortRect.height = targetRect.height;
            
            // keep in bounds
            _viewPortRect = _viewPortRect & cv::Rect(cv::Point(0,0), displayImage.size());
            
            // Copy to our viewport
            cv::Rect viewPortRectFullSize = cv::Rect(
                                                     2 * _viewPortRect.x,
                                                     2 * _viewPortRect.y,
                                                     2 * _viewPortRect.width,
                                                     2 * _viewPortRect.height);
            
            //
            // Do final display
            //
            if (FBTweakValue(@"Display", @"Layers", @"Show debug layers", NO)) {
                cv::Mat displayImageFullSize;
                cv::pyrUp(displayImage, displayImageFullSize);
                displayImageFullSize.copyTo(imageFullSize, displayImageFullSize);
            }
            
            if (FBTweakValue(@"Display", @"Viewport", @"Show zoom as outline", NO)) {
                //
                // Don't zoom, just show where we would zoom
                //
                
                cv::Rect targetViewportRectFullSize = cv::Rect(
                                                               2 * targetViewportRect.x,
                                                               2 * targetViewportRect.y,
                                                               2 * targetViewportRect.width,
                                                               2 * targetViewportRect.height);
                
                //                viewPortImageMat = displayImage;
                //                cv::rectangle(viewPortImageMat, _viewPortRect, cv::Scalar(255, 255, 0));
                
                viewPortImageMat = imageFullSize;
                cv::rectangle(viewPortImageMat, targetViewportRectFullSize, cv::Scalar(0, 255, 0));
                cv::rectangle(viewPortImageMat, viewPortRectFullSize, cv::Scalar(255, 255, 0));
            }
            else {
                //
                // Zoom to finger
                //
                viewPortImageMat = imageFullSize(viewPortRectFullSize).clone();
            }
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            if ((self.screenCaptureTimeSeconds > 0) &&
                (CACurrentMediaTime() > self.screenCaptureTimeSeconds + 1.0))
            {
                // We didn't get a good shot within a second of our goal, so give up
                self.screenCaptureTimeSeconds = 0;
            }
            
            // Do screen capture if it's time
            if ((self.screenCaptureTimeSeconds > 0) &&
                (nailBoundingRec.width > 0) &&
                (CACurrentMediaTime() > self.screenCaptureTimeSeconds) &&
                (CACurrentMediaTime() < self.screenCaptureTimeSeconds + 1.0))
            {
                [self doScreenCapture:viewPortImageMat];
                
                _nwImageView.image = nil;
                _nwImageView.backgroundColor = [UIColor whiteColor];
            }
            else {
                // Otherwise just show the image
                _nwImageView.image = [UIImage imageWithCVMat:viewPortImageMat];
            }
            
            // Screen shot countdown
            if (self.screenCaptureTimeSeconds > 0) {
                double timeleft = (self.screenCaptureTimeSeconds - CACurrentMediaTime());
                if (timeleft < 1.0f) {
                    _cameraCountdownLabel.hidden = YES;
                }
                else {
                    _cameraCountdownLabel.hidden = NO;
                    _cameraCountdownLabel.text = [NSString stringWithFormat:@"%.0f", timeleft];
                }
            }
            
            // debug messages
            _statusLabel.text = statusMessage;
            
        });
        
        // Signal that we're done with processing this frame
        dispatch_semaphore_signal(_processImageBusySemaphore);
        
    });
}

#pragma mark Photo selection
- (IBAction)selectPhoto:(UIButton *)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImageRaw = info[UIImagePickerControllerEditedImage];
    UIImage *chosenImage = [self stripUIImageTransparency:chosenImageRaw];
    
    cv::Mat tempMat = [chosenImage CVMat];
    cv::Mat tempMatBGR;
    cv::cvtColor(tempMat, tempMatBGR, CV_BGRA2BGR);
    
    cv::flip(tempMatBGR, tempMatBGR, -1); // rotate 180
    
    selectedNailImageMat = tempMatBGR.clone();
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark Screen Capture


/**
 *
 */
- (IBAction) takePhoto:(UIButton *)sender
{
    self.takeScreenCapture = YES;
    double screenCaptureDelay = 0.0; // 0 = take screen capture immediatly
    self.screenCaptureTimeSeconds = CACurrentMediaTime() + screenCaptureDelay;
}

/**
 * Called after some delay after user has tapped screen capture button
 */
- (void) doScreenCapture:(cv::Mat) image {
    _cameraCountdownLabel.hidden = YES;
    self.takeScreenCapture = NO;
    self.screenCaptureTimeSeconds = 0;
    
    self.screenCaptureImage = [UIImage imageWithCVMat:image];
    
    AudioServicesPlaySystemSound(1108);
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [self performSegueWithIdentifier:@"toShare" sender:self];
    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"No image captured" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil] show];
    }
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark iOS settings

- (BOOL) prefersStatusBarHidden{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (IBAction)actionBack:(id)sender {
    NSLog(@"%@",self.navigationController.topViewController);
    NSLog(@"%@",self.navigationController.viewControllers);
    NSLog(@"%@",self.navigationController.navigationBar);
    [self.navigationController popViewControllerAnimated:YES];
}

//MARK:- UINavigatonControllerDelegate.
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"toShare"]) {
        ShareViewController * destinationViewController = [segue destinationViewController];
        destinationViewController.shareImage = self.screenCaptureImage;
    }
}
@end
