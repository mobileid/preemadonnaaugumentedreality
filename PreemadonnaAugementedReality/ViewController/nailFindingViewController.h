//
//  nailFindingViewController.h
//  
//
//  

//

#include <opencv2/core/core.hpp>
#import <opencv2/highgui/cap_ios.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ViewController.h"
@interface nailFindingViewController : ViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate,CvVideoCameraDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *nwImageView;
@property (weak, nonatomic) IBOutlet UIImageView *neImageView;
@property (weak, nonatomic) IBOutlet UIImageView *swImageView;
@property (weak, nonatomic) IBOutlet UIImageView *seImageView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *cameraCountdownLabel;
@property (retain, nonatomic)  UIImage *defaultNailUIImage;
@property (retain, nonatomic)  UIImage *snapShotImage;
@property (nonatomic, retain) CvVideoCamera* videoCamera;
@property BOOL takeScreenCapture;
@property UIImage *screenCaptureImage;
@property double screenCaptureTimeSeconds;
- (IBAction)toggleFlash:(id)sender;
- (IBAction) selectPhoto:(UIButton *)sender;
- (IBAction) takePhoto:(UIButton *)sender;
- (IBAction) screenCapture:(UIButton *)sender;

#pragma mark : - Removed Methods
//- (void) processImage:(cv::Mat &)image;
//- (void) doScreenCapture:(cv::Mat)image;


@end
