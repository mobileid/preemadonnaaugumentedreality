//
//  SettingsViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 09/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "SettingsViewController.h"
#import "Preemadonna.h"

@interface SettingsViewController ()<UIWebViewDelegate>
{
    IBOutlet UIWebView *webView;

}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setScreeNname:@"Settings Screen"];
    // Do any additional setup after loading the view.
    
    //Terms & conditions.
        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.preemadonna.com/terms_of_service"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)actionLogout:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Logout"
                                  message:@"Are you sure?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [[Preemadonna sharedClient]Logout];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             UINavigationController * preemadonnaNavController = [self.storyboard instantiateViewControllerWithIdentifier:@"NavPreemadonnaViewController"];
                             [[UIApplication sharedApplication]keyWindow].rootViewController= preemadonnaNavController;
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)actionClearPurchase:(id)sender {
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Are you sure?"
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [[Preemadonna sharedClient].purchasedArtDataSource removeAllObjects];
                             [[Preemadonna sharedClient] save];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             [self.navigationController popViewControllerAnimated:YES];
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{

}
- (void)webViewDidStartLoad:(UIWebView *)webView{

}
- (void)webViewDidFinishLoad:(UIWebView *)webView{

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
