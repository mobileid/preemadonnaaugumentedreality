//
//  SignupViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 07/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "SignupViewController.h"
#import "ParentsSignUpViewController.h"
#import "Preemadonna.h"

@interface SignupViewController (){
    
    IBOutletCollection(UITextField) NSArray *textFieldCollection;
    IBOutlet UITextField *textFieldUsername;
    IBOutlet UITextField *textFieldEmail;
    IBOutlet UITextField *textFieldPassword;
    PFUser * user;
    IBOutlet UIVisualEffectView *viewTermsConditions;
    IBOutlet UIButton *buttonAgree;
    IBOutlet UIButton *buttonDisAgree;
    IBOutlet UIWebView *webViewTermsConditions;
    
}

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setScreeNname:@"SignUp Screen"];
    user = [[PFUser alloc] init];
    user[@"isAcceptedTermsConditions"] = @NO;
    webViewTermsConditions.delegate = self;
    [webViewTermsConditions loadHTMLString:@"<br><p><h1>Terms & Conditions</h1></p><p>This section will display content for terms & conditions </p>" baseURL:nil];
    
    viewTermsConditions.hidden = YES;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionSignUp:(id)sender {
    if ( [self isAllDataFilled]){
        if ([self NSStringIsValidEmail:textFieldEmail.text]) {
    NSTimeInterval  timeInterval = [[NSDate date] timeIntervalSinceDate:self.birthDate];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = timeInterval / secondsInAnHour;
    NSInteger daysbetweendates = hoursBetweenDates / 24;
    NSInteger years = daysbetweendates / 365;

    //409968000
    user.username = textFieldEmail.text;
    user.email = textFieldEmail.text;
    user.password = textFieldPassword.text;
    user[@"name"] = textFieldUsername.text;
    user[@"birthDate"] = self.birthDate;
    
    if ( years > 13  ) {
        if (![user[@"isAcceptedTermsConditions"]  isEqual: @YES]) {
            [self presentTermsConditions:viewTermsConditions completion:^{}];
        }else{
            [self signup];
        }
    }else if ( years < 13 ) {
        [self performSegueWithIdentifier:@"toParentsSignUp" sender:user];
    }
    }else{
    
        [[[UIAlertView alloc] initWithTitle:@"Inavlid email." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    }else{
        [[[UIAlertView alloc] initWithTitle:@"Fields are empty." message:@"Please fill all data." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

-(void)signup{
    NSLog(@"Signup");
    if ([self isAcceptedTermsConditions]) {
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (error == nil) {
                [Preemadonna sharedClient].curruntUser = user;
                [self performSegueWithIdentifier:@"toHome" sender:self];
            }else{
                [[[UIAlertView alloc] initWithTitle:@"Invalid error" message:@"Please fill all data correctly." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
        }];
    }
}

-(BOOL)isAllDataFilled{
    for (UITextField* textField in textFieldCollection) {
        if ([textField.text  isEqual: @""]) {
            return NO;
        }
    }
    return YES;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    //Email validation.
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)actionAgree:(id)sender {
    user[@"isAcceptedTermsConditions"] = @YES;
    [self hideTermsConditions:viewTermsConditions completion:^{
        viewTermsConditions.hidden  = YES;
             [self signup];
    }];
}

- (IBAction)actionDisAgree:(id)sender {
    user[@"isAcceptedTermsConditions"] = @NO;
    [self hideTermsConditions:viewTermsConditions completion:^{
    }];
}

-(BOOL)isAcceptedTermsConditions{
    return  user[@"isAcceptedTermsConditions"];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier  isEqual: @"toParentsSignUp"]) {
        ParentsSignUpViewController * parentsSignUpVC = [segue destinationViewController];
        parentsSignUpVC.user = (PFUser*)sender;
    }
}

@end
