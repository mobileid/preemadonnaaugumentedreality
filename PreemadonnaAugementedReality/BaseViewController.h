//
//  BaseViewController.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 07/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface BaseViewController : GAITrackedViewController
-(void)animatePopScalingDown:(UIView*)view;
-(void)animatePopScalingUp:(UIView*)view;
-(void)setScreeNname:(NSString*)ScreenName;
-(void)hideTermsConditions:(UIVisualEffectView*)view completion:(void(^)(void))completionHandler;
    -(void)presentTermsConditions:(UIVisualEffectView*)view completion:(void(^)(void))completionHandler;
@end
