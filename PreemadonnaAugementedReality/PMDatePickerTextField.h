//
//  PMDatePickerTextField.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 12/10/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMDatePickerTextField : UITextField
@property(nonatomic, retain) NSDate * selectedDate;
@end
