//
//  GalleryCell.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"BaseCollectionViewCell.h"
@interface GalleryCell : BaseCollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@end
