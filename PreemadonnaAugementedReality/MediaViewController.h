//
//  MediaViewController.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//
#import "BaseViewController.h"

typedef enum {
    PreeMadonnaCropOptionSquare,
    PreeMadonnaCropOptionCircle,
    PreeMadonnaCropOptionOther
} PreeMadonnaCropOptions;

@interface MediaViewController : BaseViewController
@property(nonatomic, retain) UIImage * mediaImage;
@end
