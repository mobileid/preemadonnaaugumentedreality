//
//  PMObject.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 19/10/15.
//  Copyright © 2015 LeftRightMind. All rights reserved.
//

#import "PFObject.h"
#import <Parse/Parse.h>

@interface PMObject : PFObject
-(void)saveObjectInBackgroundWithBlock:(void(^)(BOOL status, NSError * error ))block;
-(void)deleteObjectInBackgroundWithBlock:(void(^)(BOOL status, NSError * error ))block;
@end
