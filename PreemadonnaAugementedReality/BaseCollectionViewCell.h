//
//  BaseCollectionViewCell.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 14/10/15.
//  Copyright © 2015 LeftRightMind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCollectionViewCell : UICollectionViewCell

@end
