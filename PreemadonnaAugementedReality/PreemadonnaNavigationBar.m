//
//  PreemadonnaNavigationBar.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "PreemadonnaNavigationBar.h"

@implementation PreemadonnaNavigationBar


-(CGSize)sizeThatFits:(CGSize)size{
    CGSize  siZe = [super sizeThatFits:size];
    [self backgroundImageForBarPosition:UIBarPositionTop barMetrics:UIBarMetricsDefault];
    UIImage * image = [UIImage imageNamed:@"top"];
    [self setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    [self setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"Oswald-Light" size:19]}];
    return siZe;
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
