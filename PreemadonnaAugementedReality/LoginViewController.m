//
//  LoginViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 07/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "LoginViewController.h"
#import "Preemadonna.h"

@interface LoginViewController () {
    IBOutlet UITextField *textFieldPassword;
    IBOutlet UITextField *textFieldUsername;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setScreeNname:@"Login Screen"];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionLogin:(id)sender {
    // Currently Not in use.
    if (![textFieldPassword.text  isEqual: @""] && ![textFieldUsername.text  isEqual: @""]) {
    [PFUser logInWithUsernameInBackground:textFieldUsername.text password:textFieldPassword.text block:^(PFUser * _Nullable user, NSError * _Nullable error) {
        if (error == nil) {
            [Preemadonna sharedClient].curruntUser = user;
            [self performSegueWithIdentifier:@"toHome" sender:sender];
        }
    }];
    }
}

- (IBAction)actionLoginWithFacebook:(id)sender {
}

- (IBAction)actionLoginWithInstagram:(id)sender {
}

- (IBAction)actionLoginWithSnapchat:(id)sender {
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
