//
//  MediaViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "MediaViewController.h"
#import <RSKImageCropViewController.h>
#import "nailFindingViewController.h"
#import "UIImage+Circular.h"
#import "Preemadonna.h"

@interface MediaViewController ()<RSKImageCropViewControllerDataSource, RSKImageCropViewControllerDelegate>
{
    IBOutlet UIImageView *mediaImageView;
    IBOutlet UIImageView *backgroundImageView;
    PreeMadonnaCropOptions  cropOption;
    RSKImageCropViewController * imagecropVC;
    
}
@end

@implementation MediaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    mediaImageView.hidden = YES;
    backgroundImageView.image = self.mediaImage;
    mediaImageView.image = self.mediaImage;
    cropOption = PreeMadonnaCropOptionOther;
    imagecropVC =  [self initiateImageCropVc];
    [self setScreeNname:@"Media Screen"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - RSKImageCropViewControllerDataSource & RSKImageCropViewControllerDelegate
// Crop image has been canceled.
- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller
{
    [self.navigationController popViewControllerAnimated:YES];
}

// The original image has been cropped.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect
{
    mediaImageView.hidden = NO;
    backgroundImageView.alpha = 0.3;
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    mediaImageView.image = croppedImage;
    [self.navigationController popViewControllerAnimated:YES];
}

// The original image has been cropped. Additionally provides a rotation angle used to produce image.
- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect
                  rotationAngle:(CGFloat)rotationAngle
{
    mediaImageView.hidden = NO;
    backgroundImageView.alpha = 0.3;
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    [Preemadonna sharedClient].defaultImage = croppedImage;
    if (croppedImage != nil){
        if (cropOption == PreeMadonnaCropOptionCircle) {
            mediaImageView.image = [croppedImage circularScaleAndCropImage:croppedImage frame:cropRect];
        }else {
            mediaImageView.image = croppedImage;
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

// Returns a custom rect for the mask.
- (CGRect)imageCropViewControllerCustomMaskRect:(RSKImageCropViewController *)controller
{
    CGSize maskSize;
    if ([controller isPortraitInterfaceOrientation]) {
        maskSize = CGSizeMake(250, 250);
    } else {
        maskSize = CGSizeMake(220, 220);
    }
    CGFloat viewWidth = CGRectGetWidth(controller.view.frame);
    CGFloat viewHeight = CGRectGetHeight(controller.view.frame);
    CGRect maskRect = CGRectMake((viewWidth - maskSize.width) * 0.5f,
                                 (viewHeight - maskSize.height) * 0.5f,
                                 maskSize.width,
                                 maskSize.height);
    return maskRect;
}

-(void)imageCropViewController:(RSKImageCropViewController *)controller willCropImage:(UIImage *)originalImage{
    mediaImageView.hidden = NO;
    backgroundImageView.alpha = 0.3;
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    if (cropOption == PreeMadonnaCropOptionCircle) {
        mediaImageView.layer.cornerRadius = mediaImageView.frame.size.height/2;
        mediaImageView.clipsToBounds = YES;
    }else {
        mediaImageView.layer.cornerRadius = 0;
        mediaImageView.clipsToBounds = YES;
    }
}

// Returns a custom path for the mask.
- (UIBezierPath *)imageCropViewControllerCustomMaskPath:(RSKImageCropViewController *)controller
{
    CGRect rect = controller.maskRect;
    CGPoint point1 = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
    CGPoint point2 = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    CGPoint point3 = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    UIBezierPath *triangle = [UIBezierPath bezierPath];
    [triangle moveToPoint:point1];
    [triangle addLineToPoint:point2];
    [triangle addLineToPoint:point3];
    [triangle closePath];
    return triangle;
}

// Returns a custom rect in which the image can be moved.
- (CGRect)imageCropViewControllerCustomMovementRect:(RSKImageCropViewController *)controller
{
    // If the image is not rotated, then the movement rect coincides with the mask rect.
    return controller.maskRect;
}

-(RSKImageCropViewController*) initiateImageCropVc{
    RSKImageCropViewController * imageCropVC = [[RSKImageCropViewController alloc] init];
    imageCropVC.delegate = self;
    imageCropVC.dataSource = self;
    return imageCropVC;
}

#pragma mark - IBAction
- (IBAction)actionSelectSquare:(id)sender {
    imagecropVC.originalImage = backgroundImageView.image;
    imagecropVC.cropMode = RSKImageCropModeSquare;
    cropOption = PreeMadonnaCropOptionSquare;
    [self.navigationController pushViewController:imagecropVC animated:YES];
}

- (IBAction)actionSelectRound:(id)sender {
    imagecropVC.originalImage = backgroundImageView.image;
    imagecropVC.cropMode = RSKImageCropModeCircle;
    cropOption = PreeMadonnaCropOptionCircle;
    [self.navigationController pushViewController:imagecropVC animated:YES];
    
}

- (IBAction)actionGoNext:(id)sender {
    [self performSegueWithIdentifier:@"toCamera" sender:mediaImageView.image];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"toCamera"]) {
        nailFindingViewController *destinationViewController = [segue destinationViewController];
        destinationViewController.defaultNailUIImage = sender;
    }
}

@end
