//
//  BaseViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 07/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "BaseViewController.h"
#import <POP.h>
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import "GAI.h"
#import "GAITrackedViewController.h"
#import "Preemadonna.h"
@interface BaseViewController ()

- (IBAction)actionBack:(UIButton*)sender;
@property(strong, nonatomic) void (^CompletionHandler)(BOOL *status);

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton = YES;
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)actionBack:(UIButton*)sender{
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)popToNextControllerOfSegueIdentifier:(NSString*)segueIdentifier Sender:(id)sender{
    [self performSegueWithIdentifier:segueIdentifier sender:sender];
}

-(void)animatePopScalingUp:(UITableViewCell*)Cell{
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.duration = 0.1;
    scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(2, 5)];
    [Cell pop_addAnimation:scaleAnimation forKey:@"scalingUp"];
    
    POPSpringAnimation *sprintAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    sprintAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    sprintAnimation.velocity = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    sprintAnimation.springBounciness = 20.f;
    [Cell pop_addAnimation:sprintAnimation forKey:@"springAnimation"];
}
-(void)animatePopScalingDown:(UITableViewCell*)Cell{
    
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.duration = 0.1;
    scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    [Cell pop_addAnimation:scaleAnimation forKey:@"scalingDown"];
}
-(void)setScreeNname:(NSString*)ScreenName{

    id tracker = [[GAI sharedInstance] defaultTracker];
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName
           value:ScreenName];
    
    // New SDK versions
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
-(void)presentTermsConditions:(UIVisualEffectView*)view completion:(void(^)(void))completionHandler{
    view.hidden = NO;
    [self.view bringSubviewToFront:view];
    NSLog(@"show before%@",NSStringFromCGRect(view.frame));
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
       view.transform  = CGAffineTransformMakeTranslation(0.0f,25.0f);
    [UIView commitAnimations];
    completionHandler();
}
-(void)hideTermsConditions:(UIVisualEffectView*)view completion:(void(^)(void))completionHandler{
    
    view.hidden = NO;
    NSLog(@"show before%@",NSStringFromCGRect(view.frame));
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    view.transform  = CGAffineTransformMakeTranslation(0.0f,-view.frame.size.height);
    [UIView commitAnimations];
    completionHandler();
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
