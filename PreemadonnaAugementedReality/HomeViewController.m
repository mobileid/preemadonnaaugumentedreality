//
//  HomeViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "HomeViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <ELCImagePickerController.h>
#import "MediaViewController.h"
#import "Preemadonna.h"
#import "GAI.h"
#import "GAITrackedViewController.h"
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
@interface HomeViewController ()<ELCImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    ELCImagePickerController * imagePicker;
    
    
}
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //Google Analytics
    [ Preemadonna sharedClient].homeViewController = self;
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:[GAIFields customDimensionForIndex:1] value: [Preemadonna sharedClient].curruntUser.objectId];
    [tracker set:kGAIUserId value:[Preemadonna sharedClient].curruntUser.objectId];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [self setScreeNname:@"Home Screen"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ELCImagePickerControllerDelegate
-(void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info{
    
    if (info.count > 0) {
        NSDictionary * imageDictionary = info.firstObject;
        UIImage * imageUploading = imageDictionary[UIImagePickerControllerOriginalImage];
        [self dismissViewControllerAnimated:YES completion:^{
            [self performSegueWithIdentifier:@"toMediaView" sender:imageUploading];
        }];
    }
}

-(void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - IBAction
- (IBAction)actionSelectGallery:(id)sender {
    [self performSegueWithIdentifier:@"toGallery" sender:sender];
    
}

- (IBAction)actionSelectLicensedArt:(id)sender {
    [self performSegueWithIdentifier:@"toLicensedArt" sender:sender];
}

- (IBAction)actionSelectCameraRoll:(id)sender {
    imagePicker=  [[ELCImagePickerController alloc] init];
    imagePicker.imagePickerDelegate = self;
    imagePicker.maximumImagesCount = 1;
    [self presentViewController:imagePicker animated:YES completion:^{
    }];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"toMediaView"]) {
        MediaViewController *destinationViewController = [segue destinationViewController];
        destinationViewController.mediaImage = sender;
    }
}

@end
