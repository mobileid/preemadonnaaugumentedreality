//
//  LicensedArtCell.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "LicensedArtCell.h"

@implementation LicensedArtCell

-(void)prepareForReuse{
    [super prepareForReuse];
    self.iconImageView.alpha = 0.5;
}
- (IBAction)actionPurchaseLicensedArt:(id)sender {
    self.purchaseHandler(YES);
}

@end
