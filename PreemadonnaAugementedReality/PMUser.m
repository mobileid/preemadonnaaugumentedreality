//
//  PMUser.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 19/10/15.
//  Copyright © 2015 LeftRightMind. All rights reserved.
//

#import "PMUser.h"
#import "Constants.h"
#import <MBProgressHUD/MBProgressHUD.h>
@implementation PMUser

-(void)logInForPreemadonnaInBackgroundWithUsername:(NSString*) username password:(NSString *) password withBlock:(void(^)( PFUser * user,NSError * error))block{
    [MBProgressHUD showHUDAddedTo:TOPVIEW  animated:YES];
    [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser * _Nullable user, NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:TOPVIEW animated:YES];
        block(user,error);
    }];
}

-(void)signInForPreemadonnaInBackgroundWithUser:(PFUser *) user withBlock:(void(^)(BOOL status, NSError * error ))block{
    [MBProgressHUD showHUDAddedTo:TOPVIEW  animated:YES];
    [super signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:TOPVIEW animated:YES];
        block(succeeded,error);
    }];
}

-(void)savePreemadonnaUserInBackgroundWithBlock:(void(^)(BOOL status, NSError * error ))block{
    [MBProgressHUD showHUDAddedTo:TOPVIEW  animated:YES];
    [super saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:TOPVIEW  animated:YES];
        block(succeeded, error);
    }];
}
@end
