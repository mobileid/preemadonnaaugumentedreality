//
//  Preemadonna.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import <Parse/Parse.h>

#define BIG_NAIL_RECT = CGRectMake(213.875, 19.84,  28.35, 25.51);
#define BIG_NAIL_RECT_CIRCLE = CGRectMake(213.875, 19.84,  25.51, 25.51);
#define SMALL_NAIL_RECT = CGRectMake(208.92, 26.36, 20.41, 17.01);
#define SMALL_NAIL_RECT_CIRCLE = CGRectMake(208.92, 26.36,  17.01, 17.01)
#define CAMERAROLL_BIG_NAIL_RECT = CGRectMake(215.275, 21.24, 25.52, 22.68)
#define CAMERAROLL_BIG_NAIL_RECT_CIRCLE = CGRectMake(215.275, 21.24, 22.68, 22.68)
#define CAMERAROLL_SMALL_NAIL_RECT = CGRectMake(210.32, 27.76, 17.57, 14.18)
#define CAMERAROLL_SMALL_NAIL_RECT_CIRCLE = CGRectMake(210.32, 27.76, 14.18, 14.18)

@interface Preemadonna : NSObject

+(instancetype)sharedClient;
-(void)save;
-(void)Logout;
@property (nonatomic, strong) NSMutableArray *purchasedArtDataSource;
@property (nonatomic, strong) HomeViewController *homeViewController;
@property (nonatomic, strong) UIImage *defaultImage;
@property (nonatomic, strong) PFUser * curruntUser;
@property (nonatomic) BOOL isImagePickedFromCamera;

@end
