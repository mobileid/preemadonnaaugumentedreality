//
//  ShareViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 09/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "ShareViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Tweaks/FBTweak.h>
#import <Tweaks/FBTweakInline.h>
#import "Preemadonna.h"
@interface ShareViewController (){
    
    IBOutlet UIImageView *shareImageView;
    IBOutlet FBSDKShareButton *facebookShareButton;
}

@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    shareImageView.image = self.shareImage;
    [self setScreeNname:@"Share Screen"];
    [self shareImageOnFacebook];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  IBAction
- (IBAction)actionShareOnInstagram:(id)sender {
    NSString* __block imagePath;
    {
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library writeImageToSavedPhotosAlbum:[self.shareImage CGImage]
                                  orientation:(ALAssetOrientation)[self.shareImage imageOrientation]
                              completionBlock:^(NSURL *assetURL, NSError *error){
                                  if (!error) {
                                    NSString *caption = @"Check out my #AugmentedReality #NailArt with Preemadonna!";
                                      imagePath = [assetURL absoluteString];
                                      if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"instagram://app"]]) {
                                                                           NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=",[imagePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],caption]];
                                          NSLog(@"%@",instagramURL);

                                          [[UIApplication sharedApplication] openURL:instagramURL];
                                      }else{
                                      NSLog(@"%@",[[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"instagram://app"]]);
                                      
                                      }
                                      
                                  }
                                  else {
                                      // Some error
                                  }
                              }];
    }
}

- (IBAction)actionShareOnSnapchat:(id)sender {
    NSArray *objectsToShare = @[self.shareImage];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToFacebook,
                                   UIActivityTypePostToTwitter,
                                   UIActivityTypeMail,
                                   UIActivityTypeMessage,
                                   UIActivityTypePostToVimeo];
    activityVC.excludedActivityTypes = excludeActivities;
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)actionShareOnFacebook:(id)sender {
}

- (IBAction)actionShareOnPreemadonna:(id)sender {
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.preemadonna.com/"];
    NSArray *objectsToShare = @[self.shareImage, myWebsite];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToFacebook,
                                   UIActivityTypePostToTwitter,
                                   UIActivityTypeMail,
                                   UIActivityTypeMessage,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)actionPopToHome:(id)sender {
    [self.navigationController popToViewController:[Preemadonna sharedClient].homeViewController animated:YES];
}

#pragma mark - Selectore

-(void) shareImageOnFacebook{
    //MARK:- Share on Facebook
    FBSDKSharePhoto * photo = [[FBSDKSharePhoto alloc] init];
    photo.image = self.shareImage;
    photo.userGenerated = YES;
    photo.caption = @"Preemadonna";
    FBSDKSharePhotoContent * photoContent = [[FBSDKSharePhotoContent alloc] init];
    photoContent.photos = @[ photo];
    facebookShareButton.shareContent = photoContent;
    if (facebookShareButton.enabled == NO){
        
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"To upload image you need Facebook App" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil] show];
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
