//
//  SignupViewController.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 07/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//
#import "BaseViewController.h"

@interface SignupViewController : BaseViewController
@property(nonatomic,retain) NSDate * birthDate;
@end
