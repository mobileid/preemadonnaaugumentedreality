//
//  Preemadonna.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "Preemadonna.h"
#import <AutoCoding.h>
@implementation Preemadonna

+(instancetype)sharedClient
{
    static dispatch_once_t onceToken;
    static  Preemadonna * sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [Preemadonna objectWithContentsOfFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"Preemadonna"]];
        if (!sharedInstance) {
             sharedInstance = [Preemadonna new];
            if (!sharedInstance.purchasedArtDataSource)
            sharedInstance.purchasedArtDataSource = [[NSMutableArray alloc] init];
        }
    });
    return sharedInstance;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _purchasedArtDataSource = [NSMutableArray arrayWithArray:[coder decodeObjectForKey:@"purchasedArts"]];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.purchasedArtDataSource forKey:@"purchasedArts"];
}

-(void) save{
    [self writeToFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"Preemadonna"] atomically:YES];
}

-(void)Logout{
    self.curruntUser = nil;
    [PFUser logOut];
}
@end
