//
//  LicensedArtViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 08/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "LicensedArtViewController.h"
#import "LicensedArtCell.h"
#import <POP.h>
#import "nailFindingViewController.h"
#import "Preemadonna.h"
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import "GAI.h"
#import "GAITrackedViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <ELCImagePickerController.h>
#import <MessageUI/MessageUI.h>

@interface LicensedArtViewController ()< UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,ELCImagePickerControllerDelegate,UINavigationControllerDelegate,MFMailComposeViewControllerDelegate>
{
    IBOutlet UICollectionView *licencesedArtCollectionView;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIButton *buttonNext;
    LicensedArtCell * prevCell;
    LicensedArtCell * licensedArtCell;
    BOOL isAnimationFirstTime;
    BOOL isDecresedCelSize;
    NSMutableArray * imageArray;
    IBOutlet UILabel *headerLabel;
    NSMutableArray * headerArray;
    IBOutlet UIImageView *imageViewComingSoon;
    ELCImagePickerController * imagePicker;
    
}
@end

@implementation LicensedArtViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setScreeNname:@"Licenced Art Screen"];
    
    //DataSource
    imageArray = [[NSMutableArray alloc] initWithObjects:@"fbart",@"hax",@"indiegogo",@"instaart",@"pm",@"rio",@"snap",@"tc",@"tokyo",@"twitter",@"wechat",@"youtube"@"fbart",@"hax",@"indiegogo",@"instaart",@"pm",@"rio", nil];
    headerArray = [[NSMutableArray alloc] initWithObjects:@"Alexa",@"OMGhow",@"Girl Scouts New Mexico",@"Upload Photo",nil];
    
    //Delegate
    buttonNext .enabled = NO;
    buttonNext.alpha  = 4.0;
    licencesedArtCollectionView.delegate = self;
    licencesedArtCollectionView.dataSource = self;
    pageControl.numberOfPages = 4;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return headerArray.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"LicensedArtCell";
    LicensedArtCell *cell = [licencesedArtCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.iconImageView.image = [UIImage imageNamed:[imageArray objectAtIndex:indexPath.row]];
    cell.iconImageView.image.accessibilityIdentifier =[imageArray objectAtIndex:indexPath.row];
    cell.tag = indexPath.row;
    [cell setPurchaseHandler:^(BOOL *status) {
        [licencesedArtCollectionView reloadData];
        [licencesedArtCollectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    }];
    [[Preemadonna sharedClient].purchasedArtDataSource enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
        
        if (cell.tag  ==  obj.intValue) {
            cell.buttonPurchseFlag.hidden = YES;
            cell.iconImageView.alpha = 1.0;
        }
    }];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    LicensedArtCell * cell =(LicensedArtCell*) [licencesedArtCollectionView cellForItemAtIndexPath:indexPath];
    licensedArtCell = cell;
    NSString * string = cell.iconImageView.image.accessibilityIdentifier;
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:[GAIFields customDimensionForIndex:1] value:[Preemadonna sharedClient].curruntUser.objectId];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action" action:@"LicencedArtSelection" label:string value:nil]build]];
    if (prevCell.tag != cell.tag || prevCell== nil){
        
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration = 0.1;
        scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1.63, 1.63)];
        [cell pop_addAnimation:scaleAnimation forKey:@"scalingUp"];
        
        POPSpringAnimation *sprintAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        sprintAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(0.9, 0.9)];
        sprintAnimation.velocity = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
        sprintAnimation.springBounciness = 20.f;
        [cell.iconImageView pop_addAnimation:sprintAnimation forKey:@"springAnimation"];
        
        if ([Preemadonna sharedClient].purchasedArtDataSource.count == 0) {
            //setpirchas eart
            [self setPurchasingOfArt:indexPath.row];
        }
        if (![[Preemadonna sharedClient].purchasedArtDataSource containsObject:[NSNumber numberWithInt:cell.tag]]) {
            [self setPurchasingOfArt:indexPath.row];
        }
        if (prevCell != nil){
            POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
            scaleAnimation.duration = 0.1;
            scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
            [prevCell pop_addAnimation:scaleAnimation forKey:@"scalingDown"];
        }
        buttonNext.enabled = YES;
        buttonNext.alpha = 1.0;
        prevCell = cell;
        
    }else{
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration = 0.1;
        scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
        [cell pop_addAnimation:scaleAnimation forKey:@"scalingDown"];
        buttonNext.enabled = NO;
        buttonNext.alpha = 0.4;
        licensedArtCell = nil;
        prevCell = nil;
    }
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat width   = CGRectGetWidth(licencesedArtCollectionView.frame);
    pageControl.currentPage = (licencesedArtCollectionView.contentOffset.x/width);
    headerLabel.text = headerArray[pageControl.currentPage];
    if (pageControl.currentPage < headerArray.count-1) {
        NSString * nameString = [NSString stringWithFormat:@"%@comingsoon",headerArray[pageControl.currentPage]];
        imageViewComingSoon.image = [UIImage imageNamed:nameString];
    }
    else if (pageControl.currentPage == headerArray.count-1) {
        imageViewComingSoon.image = [UIImage imageNamed:@"camera_illustration"];
        
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(60, 60);
}

-(void) setPurchasingOfArt:(NSUInteger)cellNumber{
    LicensedArtCell * cell = (LicensedArtCell*)[licencesedArtCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:cellNumber inSection:0]];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Purchase the art to print using the Nailbot"
                                  message:@"This art costs $0.99"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"PURCHASE"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             cell.buttonPurchseFlag.hidden = YES;
                             cell.iconImageView.alpha = 1.0;
                             NSNumber *anumber = [NSNumber numberWithInteger:cellNumber];
                             [[Preemadonna sharedClient].purchasedArtDataSource addObject:anumber];
                             [[Preemadonna sharedClient] save];
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
                                 scaleAnimation.duration = 0.1;
                                 scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
                                 [cell pop_addAnimation:scaleAnimation forKey:@"scalingDown"];
                                 
                                 cell.buttonPurchseFlag.hidden = NO;
                                 cell.iconImageView.alpha = 0.5;
                                 buttonNext.enabled = NO;
                                 buttonNext.alpha = 0.4;
                                 prevCell = Nil;
                                 
                             }];
    [alert addAction:cancel];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark - ELCImagePickerControllerDelegate
-(void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info{
    
    if (info.count > 0) {
        NSDictionary * imageDictionary = info.firstObject;
        UIImage * imageUploading = imageDictionary[UIImagePickerControllerOriginalImage];
        [self dismissViewControllerAnimated:YES completion:^{
            [imageViewComingSoon setImage:imageUploading];
            [headerLabel setText:@"Tap to submit"];
        }];
        
    }
}

-(void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tapToUploadPhoto:(id)sender {
    
    if ([headerLabel.text  isEqual: @"Upload Photo"]) {
        imagePicker=  [[ELCImagePickerController alloc] init];
        imagePicker.imagePickerDelegate = self;
        imagePicker.maximumImagesCount = 1;
        [self presentViewController:imagePicker animated:YES completion:^{
        }];
    }else{
        [self saveImage:imageViewComingSoon.image];
        
    }
}
-(void)saveImage:(UIImage*)image{
    [self presentMailControllerWithMedia:image];
    
    PFObject * object = [PFObject objectWithClassName:@"Media"];
    NSData * imageData = UIImageJPEGRepresentation(image, 0.20);
    PFFile * file  = [PFFile fileWithData:imageData];
    object[@"image"] = file;
    object[@"imageDescription"] = @"Test";
    object[@"postedBy"] = [[Preemadonna sharedClient] curruntUser];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (error == nil) {
        }
    }];
}

-(void)presentMailControllerWithMedia:(UIImage*)image{
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController * controller = [[MFMailComposeViewController  alloc]init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Image Test"];
        [controller setToRecipients:[[NSArray alloc] initWithObjects:@"art@preemadonna.com", nil]];
        [controller setMessageBody:@" Hey Pree, \n \n Sending you my image.  \n \n Thanks!." isHTML:NO];
        [controller addAttachmentData:UIImageJPEGRepresentation(image, 0.20) mimeType:@"image/jpeg" fileName:@"image"];
        [self.navigationController presentViewController:controller animated:YES completion:nil];
    }
    else{
        
        
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(nullable NSError *)error {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - IBAction

- (IBAction)actionNext:(id)sender {
    if (licensedArtCell != nil) {
        [self performSegueWithIdentifier:@"toCamera" sender:sender];
    }
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"toCamera"]) {
        nailFindingViewController * destinationViewController = [segue destinationViewController];
        destinationViewController.defaultNailUIImage = licensedArtCell.iconImageView.image;
    }
}
@end
