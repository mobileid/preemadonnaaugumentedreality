//
//  BirthDateViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 07/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "BirthDateViewController.h"
#import "SignupViewController.h"
#import "PMDatePickerTextField.h"

@interface BirthDateViewController (){

    IBOutlet PMDatePickerTextField *textFieldBirthdate;
}
@end

@implementation BirthDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setScreeNname:@"Birthdate Screen"];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionGetBirthDate:(id)sender {
    if (![textFieldBirthdate.text isEqualToString:@""]) {
        NSLog(@"textFieldBirthdate.selectedDate%@",textFieldBirthdate.selectedDate);
        [self performSegueWithIdentifier:@"toSignUp" sender:textFieldBirthdate.selectedDate];
    }
    else{
        [[[UIAlertView alloc] initWithTitle:@"Tell us your birthday" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil] show];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"toSignUp"]) {
        SignupViewController *destinationViewController = [segue destinationViewController];
        destinationViewController.birthDate = sender;
    }
}

@end
