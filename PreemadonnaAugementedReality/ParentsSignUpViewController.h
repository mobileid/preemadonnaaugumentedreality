//
//  ParentsSignUpViewController.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 10/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "BaseViewController.h"
#import <Parse/Parse.h>
#import "PMUser.h"
@interface ParentsSignUpViewController : BaseViewController
@property(nonatomic,retain) PFUser * user;

@end
