//
//  PMDatePickerTextField.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 12/10/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "PMDatePickerTextField.h"

@implementation PMDatePickerTextField
 UIDatePicker * picker;
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 120)];
    picker.datePickerMode = UIDatePickerModeDate;
    [picker addTarget:self action:@selector(actionChangeDatePicker:) forControlEvents:UIControlEventValueChanged];
    self.inputView = picker;
}

-(void)actionChangeDatePicker:(id)sender{
    self.selectedDate = picker.date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM - dd - yyyy"];
    self.text = [formatter stringFromDate:picker.date];
}

@end
