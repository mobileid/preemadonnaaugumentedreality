//
//  ParentsSignUpViewController.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 10/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "ParentsSignUpViewController.h"
#import "PMUser.h"
#import "PMDatePickerTextField.h"
#import "Preemadonna.h"
@interface ParentsSignUpViewController ()<UIWebViewDelegate>
{
    IBOutletCollection(UITextField) NSArray *textFieldCollection;
    IBOutlet UITextField *textFieldUsername;
    IBOutlet UITextField *textFieldEmail;
    IBOutlet PMDatePickerTextField *textFieldBirthdate;
    IBOutlet UIVisualEffectView *viewTermsConditions;
    IBOutlet UIButton *buttonAgree;
    IBOutlet UIButton *buttonDisAgree;
    IBOutlet UIWebView *webViewTermsConditions;
}
@end

@implementation ParentsSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setScreeNname:@"Parent Data Screen"];
    webViewTermsConditions.delegate = self;
    [webViewTermsConditions loadHTMLString:@"<br><p><h1>Terms & Conditions</h1></p><p>This section will display content for terms & conditions </p>" baseURL:nil];
    viewTermsConditions.hidden = YES;
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)isAllDataFilled{
    
    for (UITextField* textField in textFieldCollection) {
        if ([textField.text  isEqual: @""]) {
            return NO;
        }
    }
    return YES;
    
}

- (IBAction)actionParentsSignUp:(id)sender
{
    if ( [self isAllDataFilled]){
        if ([self NSStringIsValidEmail:textFieldEmail.text]) {
            
            if (![textFieldUsername.text isEqualToString:@""] && ![textFieldEmail.text isEqualToString:@""]) {
                self.user[@"parentName"] = textFieldUsername.text;
                self.user[@"parentEmail"] = textFieldEmail.text;
                self.user[@"parentBirthday"] = textFieldBirthdate.selectedDate;
                
                if ([self isAllDataFilled] ) {
                    if (![self.user[@"isAcceptedTermsConditions"]  isEqual: @YES]) {
                        [self presentTermsConditions:viewTermsConditions completion:^{}];
                    }else{
                        [self signup];
                    }
                }
            }
        }else{
             [[[UIAlertView alloc] initWithTitle:@"Inavlid email." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }else{
        [[[UIAlertView alloc] initWithTitle:@"Fields are empty." message:@"Please fill all data." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

-(void)signup{
    NSLog(@"Signup");
    if ([self isAcceptedTermsConditions]) {
        [self.user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (error == nil) {
                [Preemadonna sharedClient].curruntUser = self.user;
                [self performSegueWithIdentifier:@"toHome" sender:self];
            }else{
                [[[UIAlertView alloc] initWithTitle:@"Invali data" message:error.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
        }];
    }
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)actionAgree:(id)sender {
    self.user[@"isAcceptedTermsConditions"] = @YES;
    
    [self hideTermsConditions:viewTermsConditions completion:^{
        viewTermsConditions.hidden  = YES;
        [self signup];
    }];
    
}

- (IBAction)actionDisAgree:(id)sender {
    self.user[@"isAcceptedTermsConditions"] = @NO;
    
    [self hideTermsConditions:viewTermsConditions completion:^{
    }];
    
    
}
-(BOOL)isAcceptedTermsConditions{
    return  self.user[@"isAcceptedTermsConditions"];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
