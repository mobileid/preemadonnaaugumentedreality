//
//  PMObject.m
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 19/10/15.
//  Copyright © 2015 LeftRightMind. All rights reserved.
//

#import "PMObject.h"
#import "Constants.h"
#import <MBProgressHUD/MBProgressHUD.h>

@implementation PMObject
-(void)saveObjectInBackgroundWithBlock:(void(^)(BOOL status, NSError * error ))block{
    [MBProgressHUD showHUDAddedTo:TOPVIEW  animated:YES];
    [super saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:TOPVIEW  animated:YES];
        block(succeeded, error);
    }];
}

-(void)deleteObjectInBackgroundWithBlock:(void(^)(BOOL status, NSError * error ))block{
    [MBProgressHUD showHUDAddedTo:TOPVIEW  animated:YES];
    [super deleteInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:TOPVIEW  animated:YES];
        block(succeeded, error);
    }];
}

@end
