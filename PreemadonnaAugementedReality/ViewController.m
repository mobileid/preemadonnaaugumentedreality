//
//  ViewController.m
//  Ambassador
//
//  Created by Stan James on 7/2/15.
//  Copyright (c) 2015 Preemadonna. All rights reserved.
//

#import "ViewController.h"
#import <Tweaks/FBTweak.h>
#import <Tweaks/FBTweakInline.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
