//
//  ShareViewController.h
//  PreemadonnaAugementedReality
//
//  Created by LeftRightMind on 09/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

#import "BaseViewController.h"

@interface ShareViewController : BaseViewController

@property(nonatomic,retain) UIImage * shareImage;
@property (nonatomic, retain) UIDocumentInteractionController *dic;    
@end
